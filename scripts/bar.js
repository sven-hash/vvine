// Open file
// Get selected country data
// Update graphe

var ctx = document.getElementById('myBarChart').getContext('2d');
var myBarChart;

var type = 0;
var country = "";

initChartBar();

function countryChangedBar(n_country) {
    country = n_country;
    updateDatas();
}

function typeChangedBar(n_type) {
    type = n_type;

    let asc = "select-button not-selected-button"
    let dsc = "select-button selected-button"

    if(type == 0){
        asc = "select-button selected-button"
        dsc = "select-button not-selected-button"
    }

    document.getElementById('btnASC').className = asc;
    document.getElementById('btnDSC').className = dsc;

    updateDatas();
}

function initChartBar() {

    myBarChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: [],
            datasets: [{
                backgroundColor: "#FFC0CB",
                data: [],
                borderWidth: 1,
                categoryPercentage: 1.0,
                barPercentage: 0.9,
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                callbacks: {
                    label: (item) => `${item.xLabel} $`,
                    title: (item) => ``,
                },
                displayColors: false,
            },
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ordered: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Price'
                    }
                }],
                yAxes: [{
                    ticks: {
                        mirror: true,
                        padding: -5,
                        fontColor: "#000",
                        z: 2
                    },
                    gridLines: false
                }]
            },
        }
    });

}

function updateChartBar(datas, labels) {

    myBarChart.data.datasets[0].data = datas;
    myBarChart.data.labels = labels;
    myBarChart.update();
}

function updateDatas() {

    // Liste des vins
    // Trier
    // Séparer data
    // Update graphe

    let wines = barData[country];
    wines.sort(function compareWine(a, b) {
        if (type == 0) {
            return a.price - b.price;
        } else {
            return b.price - a.price;
        }
        return a - b;
    });

    datas = [];
    labels = [];

    let count = 0;
    for (var item in wines) {
        wine = wines[item];
        labels.push(wine.title + " - " + wine.points + " points");
        datas.push(wine.price);

        count = count + 1;
        if (count >= 10) break;
    }
    updateChartBar(datas, labels);
}
