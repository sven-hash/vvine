function showPopup(choice){

    var popup = document.getElementById('info-popup');

    if (choice) {
        popup.style.display = "block";
    } else {
        popup.style.display = "none";
    }

}
