
var ctxLine = document.getElementById('myLineChart');
var myLineChart;


var winesYear;
var winesGrades;
var wineIndex = 0;
var labels = [];
var data = [];
var country = '';
var winesSelected;
var sliderYear;

initChartLine(country);


function initChartLine(country) {



    var textLine = '';

    if (country != '')
        textLine = 'Best Wines of all time for ' + country;


    myLineChart = new Chart(ctxLine, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                data: data,
                label: "Average grades",
                borderColor: "#7f1a1a",
                backgroundColor: "#7f1a1a",
                pointStyle: 'circle',
                pointBackgroundColor: "#7f1a1a",
                pointRadius: 4,
                fill: false
            },
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false,

            },
            tooltips: {
                callbacks: {
                    title: (item) => ``,
                },
                displayColors: false,
            },
            scales: {
                xAxes: [{
                    gridLines: false,
                    scaleLabel: {
                        display: true,
                        labelString: 'Year'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Average grades'
                    }
                }]

            },

        }
    });
}


function findIndexCountry(country) {
    var i = 0;
    for (; i < dataLine.length; i++) {
        if (dataLine[i]['country'] == country)
            break;
    }
    return i;
}

function countryChangedLine(country) {
    wineIndex = findIndexCountry(country);
    winesSelected = dataLine[wineIndex];

    labels = [];
    data = [];

    for (name in winesSelected) {
        if (name != 'country') {
            if (winesSelected[name] != 0) {
                labels.push(name);
                data.push(winesSelected[name]);
            }
        }
    }

    var minYear = Math.min(...labels);
    var maxYear = Math.max(...labels);
    initSlider(minYear, maxYear);
    updateChartLine();

}

function initSlider(min, max) {
    $(".js-range-slider").ionRangeSlider({
        type: "double",
        skin: "round",
        prettify_enabled: false,
        min: min,
        max: max,
        from: min,
        to: max,
        grid: false,

        onFinish: function (dataSlider) {
            // Called then action is done and mouse is released
            labels = []
            data = []
            for (year in winesSelected) {
                if (year != 'country') {
                    if (winesSelected[year] != 0) {
                        if (year >= dataSlider.from && year <= dataSlider.to) {
                            labels.push(year);
                            data.push(winesSelected[year]);
                        }
                    }
                }
            }

            updateChartLine();

        },

    });
    let sliderYear = $(".js-range-slider").data("ionRangeSlider");
    sliderYear.update({
        min: min,
        max: max,
        from: min,
        to: max
    });

}


function updateChartLine() {

    myLineChart.data.datasets[0].data = data;
    myLineChart.data.labels = labels;
    myLineChart.update();

}
