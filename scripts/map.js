document.addEventListener('DOMContentLoaded', function () {
    Highcharts.mapChart('chartdiv', {
        chart: {
            map: 'custom/world',
            backgroundColor: "rgba(0,0,0,0)",
        },

        legend: {
            enabled: false,
        },


        title: {
            text: null
        },

        credits: {
            enabled: true,
            href: "https://www.kaggle.com/zynicide/wine-reviews",
            mapText: "",
            text: "Data source: Kaggle wine review dataset; Retrieved: 06.11.2020"
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            type: "logarithmic",
            stops: [
                [0.2, '#FFC0CB'],
                [0.8, '#7f1a1a']
            ],

        },

        series: [{
            data: mapdata,
            name: 'Number of wines',
            borderColor: "#7C7C7C",
            allowPointSelect: true,
            point: {
                events: {
                    click: function (e) {
                        if (e.point.state == 'select') {
                            event.preventDefault();
                        }
                        else {
                            var countryname = e.point.name;
                            if (countryname == "United States of America") {
                                countryname = "US";
                            }
                            else if (countryname == "United Kingdom") {
                                countryname = "England"
                            }
                            document.getElementById("mapTitle").textContent = countryname + " - " + e.point.value + " wine" + (e.point.value == 1 ? "" : "s") + " registred";

                            countryChangedBar(countryname);
                            countryChangedLine(countryname);
                            countryChangedPie(countryname);
                        }
                    }
                }
            },
            states: {
                normal: {
                    animation: {
                        duration: 100,
                    }
                },
                select: {
                    color: "#F3DB74",
                    animation: {
                        duration: 100,
                    }
                },
                waiting: {
                    opacity: 0.2,
                    animation: {
                        duration: 100,
                    }
                }
            }
        }]
    });
});
