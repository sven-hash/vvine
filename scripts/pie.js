// Open file
// Get selected country data
// Update graphe

var ctx = document.getElementById('myPieChart').getContext('2d');
var ctx2 = document.getElementById('myPieChart2').getContext('2d');
var myPieChart;
var myPieChart2;

var country = "";

initChartPie();


function findIndexCountryPie(country) {
    var i = 0;
    for (; i < pieData.length; i++) {
        if (pieData[i]['country'] == country)
            break;
    }
    return i;
}

function countryChangedPie(n_country) {
    country = n_country;
    updateDataPie();
}

function initChartPie() {

    myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: [],
            datasets: [{
                backgroundColor: [],
                data: [],
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Most represented variety'
            },
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                displayColors: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString() + ' %';
                        return value;
                    }
                }
            },
            legend: {
                display: true,
                position: 'bottom'
            },
        }
    });

    myPieChart2 = new Chart(ctx2, {
        type: 'doughnut',
        data: {
            labels: [],
            datasets: [{
                backgroundColor: [],
                data: [],
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Most represented province'
            },
            responsive: true,
            maintainAspectRatio: false,
            tooltips: {
                displayColors: false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString() + ' %';
                        return value;
                    }
                }
            },
            legend: {
                display: true,
                position: 'bottom'
            },
        }
    });

}

function updateChartPie(datas, labels, datas2, labels2, backgroundColor) {

    myPieChart.data.datasets[0].data = datas;
    myPieChart.data.labels = labels;
    myPieChart.data.datasets[0].backgroundColor = backgroundColor;
    myPieChart.update();

    myPieChart2.data.datasets[0].data = datas2;
    myPieChart2.data.labels = labels2;
    myPieChart2.data.datasets[0].backgroundColor = backgroundColor;
    myPieChart2.update();
}

function updateDataPie() {
    wineIndex = findIndexCountryPie(country);

    backgroundColor = ["#7f1a1a", "#ffc0cb"];
    datas = [pieData[wineIndex]['percent'], 100 - pieData[wineIndex]['percent']];
    labels = [pieData[wineIndex]['variety'], 'Others'];

    datas2 = [pieData[wineIndex]['prov_percent'], 100 - pieData[wineIndex]['prov_percent']];
    labels2 = [pieData[wineIndex]['province'], 'Others'];

    updateChartPie(datas, labels, datas2, labels2, backgroundColor);
}
