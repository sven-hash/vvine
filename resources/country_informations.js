var pieData = [
    {
        "country": "Argentina",
        "nb_var": 30,
        "variety": "Malbec",
        "percent": 48,
        "province": "Mendoza Province",
        "prov_percent": 85
      },
      {
        "country": "Armenia",
        "nb_var": 2,
        "variety": "Rosé",
        "percent": 50,
        "province": "Armenia",
        "prov_percent": 100
      },
      {
        "country": "Australia",
        "nb_var": 32,
        "variety": "Shiraz",
        "percent": 30,
        "province": "South Australia",
        "prov_percent": 57
      },
      {
        "country": "Austria",
        "nb_var": 24,
        "variety": "Grüner Veltliner",
        "percent": 45,
        "province": "Burgenland",
        "prov_percent": 20
      },
      {
        "country": "Bosnia and Herzegovina",
        "nb_var": 2,
        "variety": "BlatinaVranec",
        "percent": 50,
        "province": "Mostar",
        "prov_percent": 100
      },
      {
        "country": "Brazil",
        "nb_var": 11,
        "variety": "Sparkling Blend",
        "percent": 17,
        "province": "Serra Gaúcha",
        "prov_percent": 34
      },
      {
        "country": "Bulgaria",
        "nb_var": 17,
        "variety": "Red Blend",
        "percent": 17,
        "province": "Thracian Valley",
        "prov_percent": 78
      },
      {
        "country": "Canada",
        "nb_var": 20,
        "variety": "Riesling",
        "percent": 27,
        "province": "British Columbia",
        "prov_percent": 62
      },
      {
        "country": "Chile",
        "nb_var": 31,
        "variety": "Cabernet Sauvignon",
        "percent": 18,
        "province": "Colchagua Valley",
        "prov_percent": 18
      },
      {
        "country": "China",
        "nb_var": 1,
        "variety": "Cabernet Blend",
        "percent": 100,
        "province": "China",
        "prov_percent": 100
      },
      {
        "country": "Croatia",
        "nb_var": 17,
        "variety": "Plavac Mali",
        "percent": 25,
        "province": "Istria",
        "prov_percent": 31
      },
      {
        "country": "Cyprus",
        "nb_var": 4,
        "variety": "White Blend",
        "percent": 45,
        "province": "PafosCyprus",
        "prov_percent": 27
      },
      {
        "country": "Czech Republic",
        "nb_var": 9,
        "variety": "Red Blend",
        "percent": 17,
        "province": "Moravia",
        "prov_percent": 100
      },
      {
        "country": "England",
        "nb_var": 4,
        "variety": "Sparkling Blend",
        "percent": 64,
        "province": "England",
        "prov_percent": 100
      },
      {
        "country": "France",
        "nb_var": 40,
        "variety": "Bordeaux-style Red Blend",
        "percent": 25,
        "province": "Bordeaux",
        "prov_percent": 26
      },
      {
        "country": "Georgia",
        "nb_var": 8,
        "variety": "Saperavi",
        "percent": 42,
        "province": "Kakheti",
        "prov_percent": 84
      },
      {
        "country": "Germany",
        "nb_var": 13,
        "variety": "Riesling",
        "percent": 92,
        "province": "Mosel",
        "prov_percent": 47
      },
      {
        "country": "Greece",
        "nb_var": 23,
        "variety": "White Blend",
        "percent": 17,
        "province": "Peloponnese",
        "prov_percent": 12
      },
      {
        "country": "Hungary",
        "nb_var": 15,
        "variety": "Furmint",
        "percent": 40,
        "province": "Tokaj",
        "prov_percent": 32
      },
      {
        "country": "India",
        "nb_var": 3,
        "variety": "Shiraz",
        "percent": 44,
        "province": "Nashik",
        "prov_percent": 100
      },
      {
        "country": "Israel",
        "nb_var": 27,
        "variety": "Cabernet Sauvignon",
        "percent": 23,
        "province": "Galilee",
        "prov_percent": 53
      },
      {
        "country": "Italy",
        "nb_var": 34,
        "variety": "Red Blend",
        "percent": 30,
        "province": "Tuscany",
        "prov_percent": 30
      },
      {
        "country": "Lebanon",
        "nb_var": 6,
        "variety": "Red Blend",
        "percent": 46,
        "province": "Bekaa Valley",
        "prov_percent": 62
      },
      {
        "country": "Luxembourg",
        "nb_var": 4,
        "variety": "Riesling",
        "percent": 33,
        "province": "Moselle Luxembourgeoise",
        "prov_percent": 100
      },
      {
        "country": "Macedonia",
        "nb_var": 8,
        "variety": "Pinot Noir",
        "percent": 25,
        "province": "Tikves",
        "prov_percent": 100
      },
      {
        "country": "Mexico",
        "nb_var": 17,
        "variety": "Red Blend",
        "percent": 22,
        "province": "Valle de Guadalupe",
        "prov_percent": 94
      },
      {
        "country": "Moldova",
        "nb_var": 17,
        "variety": "Red Blend",
        "percent": 22,
        "province": "Moldova",
        "prov_percent": 91
      },
      {
        "country": "Morocco",
        "nb_var": 8,
        "variety": "Red Blend",
        "percent": 29,
        "province": "Zenata",
        "prov_percent": 67
      },
      {
        "country": "New Zealand",
        "nb_var": 20,
        "variety": "Sauvignon Blanc",
        "percent": 41,
        "province": "Marlborough",
        "prov_percent": 56
      },
      {
        "country": "Peru",
        "nb_var": 11,
        "variety": "Red Blend",
        "percent": 25,
        "province": "Ica",
        "prov_percent": 100
      },
      {
        "country": "Portugal",
        "nb_var": 26,
        "variety": "Portuguese Red",
        "percent": 70,
        "province": "Douro",
        "prov_percent": 22
      },
      {
        "country": "Romania",
        "nb_var": 14,
        "variety": "Cabernet Sauvignon",
        "percent": 16,
        "province": "Dealu Mare",
        "prov_percent": 25
      },
      {
        "country": "Serbia",
        "nb_var": 6,
        "variety": "Red Blend",
        "percent": 25,
        "province": "Župa",
        "prov_percent": 66
      },
      {
        "country": "Slovakia",
        "nb_var": 1,
        "variety": "Riesling",
        "percent": 100,
        "province": "Muzla",
        "prov_percent": 100
      },
      {
        "country": "Slovenia",
        "nb_var": 19,
        "variety": "White Blend",
        "percent": 14,
        "province": "Goriska Brda",
        "prov_percent": 29
      },
      {
        "country": "South Africa",
        "nb_var": 26,
        "variety": "Chenin Blanc",
        "percent": 15,
        "province": "Stellenbosch",
        "prov_percent": 31
      },
      {
        "country": "Spain",
        "nb_var": 36,
        "variety": "Tempranillo",
        "percent": 29,
        "province": "Northern Spain",
        "prov_percent": 57
      },
      {
        "country": "Switzerland",
        "nb_var": 5,
        "variety": "Pinot Noir",
        "percent": 43,
        "province": "Switzerland",
        "prov_percent": 42
      },
      {
        "country": "Turkey",
        "nb_var": 13,
        "variety": "Red Blend",
        "percent": 29,
        "province": "Aegean",
        "prov_percent": 24
      },
      {
        "country": "Ukraine",
        "nb_var": 8,
        "variety": "Sparkling Blend",
        "percent": 43,
        "province": "Ukraine",
        "prov_percent": 100
      },
      {
        "country": "Uruguay",
        "nb_var": 12,
        "variety": "Tannat",
        "percent": 54,
        "province": "Canelones",
        "prov_percent": 39
      },
      {
        "country": "US",
        "nb_var": 58,
        "variety": "Pinot Noir",
        "percent": 21,
        "province": "California",
        "prov_percent": 66
      }
];
