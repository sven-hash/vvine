var barData =
  {
    "Argentina": [
      {
        "title": "Bodega Catena Zapata 2006 Nicasia Vineyard Malbec (Mendoza)",
        "points": 97,
        "price": 120,
        "ratio": 0.81
      },
      {
        "title": "Alta Vista 2013 Single Vineyard Temis Malbec (Mendoza)",
        "points": 96,
        "price": 48,
        "ratio": 2
      },
      {
        "title": "Riglos 2009 Gran Corte Las Divas Vineyard Red (Tupungato)",
        "points": 96,
        "price": 50,
        "ratio": 1.92
      },
      {
        "title": "Alta Vista 2012 Single Vineyard Temis Malbec (Valle de Uco)",
        "points": 95,
        "price": 48,
        "ratio": 1.98
      },
      {
        "title": "Vistalba 2007 Corte A Red (Mendoza)",
        "points": 95,
        "price": 50,
        "ratio": 1.9
      },
      {
        "title": "Trapiche 2009 Finca Jorge Miralles Single Vineyard Malbec (La Consulta)",
        "points": 95,
        "price": 50,
        "ratio": 1.9
      },
      {
        "title": "Finca Perdriel 2007 Vineyard Selection Red (Mendoza)",
        "points": 95,
        "price": 62,
        "ratio": 1.53
      },
      {
        "title": "Pascual Toso 2014 Finca Pedregal Single Vineyard Barrancas Maipú Selected Lots Cabernet Sauvignon-Malbec (Mendoza)",
        "points": 95,
        "price": 74,
        "ratio": 1.28
      },
      {
        "title": "Colomé 2010 Reserva Malbec (Salta)",
        "points": 95,
        "price": 90,
        "ratio": 1.06
      },
      {
        "title": "Terrazas de Los Andes 2009 Single Parcel Los Cerezos Malbec (Luján de Cuyo)",
        "points": 95,
        "price": 100,
        "ratio": 0.95
      },
      {
        "title": "Viña Cobos 2010 Cobos Marchiori Vineyard Malbec (Mendoza)",
        "points": 95,
        "price": 225,
        "ratio": 0.42
      },
      {
        "title": "Melipal 2012 Nazarenas Vineyard Malbec (Mendoza)",
        "points": 94,
        "price": 40,
        "ratio": 2.35
      },
      {
        "title": "Viña Cobos 2010 Bramare Malbec (Luján de Cuyo)",
        "points": 94,
        "price": 44,
        "ratio": 2.14
      },
      {
        "title": "Alta Vista 2012 Single Vineyard Alizarine Malbec (Luján de Cuyo)",
        "points": 94,
        "price": 48,
        "ratio": 1.96
      },
      {
        "title": "Alta Vista 2012 Single Vineyard Serenade Malbec (Luján de Cuyo)",
        "points": 94,
        "price": 48,
        "ratio": 1.96
      },
      {
        "title": "Alta Vista 2013 Single Vineyard Serenade Malbec (Mendoza)",
        "points": 94,
        "price": 48,
        "ratio": 1.96
      },
      {
        "title": "Alta Vista 2006 Single Vineyard Temis Malbec (Uco Valley)",
        "points": 94,
        "price": 50,
        "ratio": 1.88
      },
      {
        "title": "Monteviejo 2009 Lindaflor Red (Uco Valley)",
        "points": 94,
        "price": 50,
        "ratio": 1.88
      },
      {
        "title": "Trapiche 2009 Finca Domingo F. Sarmiento Single Vineyard Malbec (La Consulta)",
        "points": 94,
        "price": 50,
        "ratio": 1.88
      },
      {
        "title": "Alta Vista 2010 Temis Single Vineyard Malbec (Mendoza)",
        "points": 94,
        "price": 50,
        "ratio": 1.88
      }
    ],
    "Armenia": [
      {
        "title": "Van Ardi 2015 Estate Bottled Rosé (Armenia)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Van Ardi 2015 Estate Bottled Kangoun (Armenia)",
        "points": 87,
        "price": 14,
        "ratio": 6.21
      }
    ],
    "Australia": [
      {
        "title": "Chambers Rosewood Vineyards NV Rare Muscat (Rutherglen)",
        "points": 100,
        "price": 350,
        "ratio": 0.29
      },
      {
        "title": "Chambers Rosewood Vineyards NV Rare Muscadelle (Rutherglen)",
        "points": 99,
        "price": 300,
        "ratio": 0.33
      },
      {
        "title": "Penfolds 2010 Grange Shiraz (South Australia)",
        "points": 99,
        "price": 850,
        "ratio": 0.12
      },
      {
        "title": "R.L. Buller & Son NV Calliope Rare Tokay (Rutherglen)",
        "points": 98,
        "price": 86,
        "ratio": 1.14
      },
      {
        "title": "Torbreck 2013 The Factor Shiraz (Barossa Valley)",
        "points": 98,
        "price": 125,
        "ratio": 0.78
      },
      {
        "title": "Standish 2006 The Relic Shiraz (Barossa Valley)",
        "points": 98,
        "price": 135,
        "ratio": 0.73
      },
      {
        "title": "Penfolds 2008 Grange Shiraz (South Australia)",
        "points": 98,
        "price": 850,
        "ratio": 0.12
      },
      {
        "title": "Standish 2012 Andelmonde Shiraz (Barossa Valley)",
        "points": 97,
        "price": 95,
        "ratio": 1.02
      },
      {
        "title": "Chambers Rosewood Vineyards NV Grand Muscat (Rutherglen)",
        "points": 97,
        "price": 100,
        "ratio": 0.97
      },
      {
        "title": "Torbreck 2012 RunRig Shiraz-Viognier (Barossa)",
        "points": 97,
        "price": 225,
        "ratio": 0.43
      },
      {
        "title": "D'Arenberg 2010 The Swinging Malaysian Single Vineyard Shiraz (McLaren Vale)",
        "points": 96,
        "price": 85,
        "ratio": 1.13
      },
      {
        "title": "Thorn Clarke 2012 Ron Thorn Single Vineyard Shiraz (Barossa)",
        "points": 96,
        "price": 89,
        "ratio": 1.08
      },
      {
        "title": "Glaetzer 2010 Amon-Ra Unfiltered Shiraz (Barossa Valley)",
        "points": 96,
        "price": 110,
        "ratio": 0.87
      },
      {
        "title": "Kay Brothers 2012 Amery Vineyard Block 6 Shiraz (McLaren Vale)",
        "points": 96,
        "price": 120,
        "ratio": 0.8
      },
      {
        "title": "Standish 2005 The Standish Single Vineyard Shiraz (Barossa Valley)",
        "points": 96,
        "price": 135,
        "ratio": 0.71
      },
      {
        "title": "Penfolds 2009 RWT Shiraz (Barossa Valley)",
        "points": 96,
        "price": 150,
        "ratio": 0.64
      },
      {
        "title": "Hickinbotham 2013 The Peake Cabernet-Shiraz (McLaren Vale)",
        "points": 96,
        "price": 150,
        "ratio": 0.64
      },
      {
        "title": "Penfolds 1996 Grange Shiraz (South Australia)",
        "points": 96,
        "price": 185,
        "ratio": 0.52
      },
      {
        "title": "Torbreck 2007 RunRig Shiraz-Viognier (Barossa Valley)",
        "points": 96,
        "price": 225,
        "ratio": 0.43
      },
      {
        "title": "Penfolds 2004 Grange Shiraz (South Australia)",
        "points": 96,
        "price": 500,
        "ratio": 0.19
      }
    ],
    "Austria": [
      {
        "title": "Kracher 2010 Nouvelle Vague Nummer 9 Trockenbeerenauslese Chardonnay (Burgenland)",
        "points": 97,
        "price": 90,
        "ratio": 1.08
      },
      {
        "title": "Kracher 2013 Trockenbeerenauslese Nummer 5 Zwischen den Seen Scheurebe (Burgenland)",
        "points": 97,
        "price": 95,
        "ratio": 1.02
      },
      {
        "title": "Kracher 2013 Grande Cuvée Trockenbeerenauslese Nummer 6 Nouvelle Vague White (Burgenland)",
        "points": 97,
        "price": 95,
        "ratio": 1.02
      },
      {
        "title": "Kracher 2008 Zwischen den Seen Nummer 11 Trockenbeerenauslese Scheurebe (Burgenland)",
        "points": 97,
        "price": 103,
        "ratio": 0.94
      },
      {
        "title": "Kracher 2010 Zwischen den Seen Nummer 10 Trockenbeerenauslese Scheurebe (Burgenland)",
        "points": 97,
        "price": 125,
        "ratio": 0.78
      },
      {
        "title": "Eichinger 2014 Gaisberg Reserve Riesling (Kamptal)",
        "points": 96,
        "price": 28,
        "ratio": 3.43
      },
      {
        "title": "Bründlmayer 2013 Heiligenstein Reserve Lyra Riesling (Kamptal)",
        "points": 96,
        "price": 35,
        "ratio": 2.74
      },
      {
        "title": "Salomon-Undhof 2009 Bienenfeld Beerenauslese Riesling (Niederösterreich)",
        "points": 96,
        "price": 38,
        "ratio": 2.53
      },
      {
        "title": "Emmerich Knoll 2013 Loibner Auslese Grüner Veltliner (Wachau)",
        "points": 96,
        "price": 40,
        "ratio": 2.4
      },
      {
        "title": "Kracher 2012 Nouvelle Vague Nr. 6 Grande Cuvée Trockenbeerenauslese White (Burgenland)",
        "points": 96,
        "price": 45,
        "ratio": 2.13
      },
      {
        "title": "Schloss Gobelsburg 2015 Eiswein Grüner Veltliner (Niederösterreich)",
        "points": 96,
        "price": 45,
        "ratio": 2.13
      },
      {
        "title": "Anton Bauer 2012 Reserve Limited Edition Pinot Noir (Wagram)",
        "points": 96,
        "price": 50,
        "ratio": 1.92
      },
      {
        "title": "Emmerich Knoll 2013 Ried Loibenberg Smaragd Riesling (Wachau)",
        "points": 96,
        "price": 54,
        "ratio": 1.78
      },
      {
        "title": "Emmerich Knoll 2014 Ried Loibenberg Smaragd Riesling (Wachau)",
        "points": 96,
        "price": 56,
        "ratio": 1.71
      },
      {
        "title": "Bründlmayer 2014 Heiligenstein Lyra Reserve Riesling (Kamptal)",
        "points": 96,
        "price": 64,
        "ratio": 1.5
      },
      {
        "title": "Emmerich Knoll 2014 Ried Kellerberg Smaragd Riesling (Wachau)",
        "points": 96,
        "price": 64,
        "ratio": 1.5
      },
      {
        "title": "Malat 2014 Das Beste vom Riesling Reserve Riesling (Kremstal)",
        "points": 96,
        "price": 70,
        "ratio": 1.37
      },
      {
        "title": "Bründlmayer 2011 Zöbinger Heiligenstein Erste Lage Alte Reben Reserve Riesling (Kamptal)",
        "points": 96,
        "price": 78,
        "ratio": 1.23
      },
      {
        "title": "Emmerich Knoll 2014 Ried Schütt Smaragd Riesling (Wachau)",
        "points": 96,
        "price": 78,
        "ratio": 1.23
      },
      {
        "title": "Bründlmayer 2010 Zöbinger Heiligenstein Erste Lage Alte Reben Reserve Riesling (Kamptal)",
        "points": 96,
        "price": 80,
        "ratio": 1.2
      }
    ],
    "Bosnia and Herzegovina": [
      {
        "title": "Winery ?itluk 2011 Blatina (Mostar)",
        "points": 88,
        "price": 12,
        "ratio": 7.33
      },
      {
        "title": "Winery ?itluk 2007 Vranac Vranec (Mostar)",
        "points": 85,
        "price": 13,
        "ratio": 6.54
      }
    ],
    "Brazil": [
      {
        "title": "Cave Geisse 2013 Brut Nature Sparkling (Pinto Bandeira)",
        "points": 89,
        "price": 36,
        "ratio": 2.47
      },
      {
        "title": "Cave Geisse 2012 Blanc de Blancs Brut Chardonnay (Pinto Bandeira)",
        "points": 89,
        "price": 45,
        "ratio": 1.98
      },
      {
        "title": "Lidio Carraro 2011 Agnus Merlot (Serra Gaúcha)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Lidio Carraro 2013 Dádivas Chardonnay (Serra Gaúcha)",
        "points": 88,
        "price": 20,
        "ratio": 4.4
      },
      {
        "title": "Casa Perini NV Moscatel (Vale Trentino)",
        "points": 88,
        "price": 20,
        "ratio": 4.4
      },
      {
        "title": "Casa Valduga NV 130 Brut  (Brazil)",
        "points": 88,
        "price": 33,
        "ratio": 2.67
      },
      {
        "title": "Cave Geisse 2014 Brut Sparkling (Pinto Bandeira)",
        "points": 88,
        "price": 36,
        "ratio": 2.44
      },
      {
        "title": "Casa Perini NV Charmat Brut Rosé Sparkling (Vale Trentino)",
        "points": 87,
        "price": 20,
        "ratio": 4.35
      },
      {
        "title": "Salton 2009 Talento Red (Brazil)",
        "points": 87,
        "price": 21,
        "ratio": 4.14
      },
      {
        "title": "Cave Geisse NV Cave Amadeu Rosé Brut Pinot Noir (Pinto Bandeira)",
        "points": 87,
        "price": 25,
        "ratio": 3.48
      },
      {
        "title": "Lidio Carraro 2014 Agnus Tannat (Serra Gaúcha)",
        "points": 86,
        "price": 12,
        "ratio": 7.17
      },
      {
        "title": "Casa Valduga NV Brut 130  (Vale dos Vinhedos)",
        "points": 86,
        "price": 20,
        "ratio": 4.3
      },
      {
        "title": "Casa Valduga 2015 Blush  (Brazil)",
        "points": 86,
        "price": 23,
        "ratio": 3.74
      },
      {
        "title": "Cave Geisse NV Cave Amadeu Moscatel (Pinto Bandeira)",
        "points": 86,
        "price": 25,
        "ratio": 3.44
      },
      {
        "title": "Miolo 2012 Millésime Brut  (Vale dos Vinhedos)",
        "points": 86,
        "price": 30,
        "ratio": 2.87
      },
      {
        "title": "Villaggio Grando NV Innominabile Lote IV Red (Santa Catarina)",
        "points": 86,
        "price": 35,
        "ratio": 2.46
      },
      {
        "title": "Miolo 2011 Lote 43 Merlot-Cabernet Sauvignon (Vale dos Vinhedos)",
        "points": 86,
        "price": 45,
        "ratio": 1.91
      },
      {
        "title": "Casa Perini 2012 Quatro Red (Vale Trentino)",
        "points": 86,
        "price": 60,
        "ratio": 1.43
      },
      {
        "title": "Lidio Carraro 2014 Agnus Merlot (Serra Gaúcha)",
        "points": 85,
        "price": 12,
        "ratio": 7.08
      },
      {
        "title": "Casa Perini 2014 Macaw Cabernet Sauvignon (Serra Gaúcha)",
        "points": 85,
        "price": 15,
        "ratio": 5.67
      }
    ],
    "Bulgaria": [
      {
        "title": "Chateau Burgozone 2012 Collection Chardonnay (Danube River Plains)",
        "points": 91,
        "price": 23,
        "ratio": 3.96
      },
      {
        "title": "Castra Rubra 2011 Butterfly's Rock Red (Thracian Valley)",
        "points": 91,
        "price": 25,
        "ratio": 3.64
      },
      {
        "title": "Castra Rubra 2010 CR Red (Thracian Valley)",
        "points": 91,
        "price": 30,
        "ratio": 3.03
      },
      {
        "title": "Bessa 2011 Grande Cuvée Red (Thracian Valley)",
        "points": 91,
        "price": 55,
        "ratio": 1.65
      },
      {
        "title": "K Cellars 2011 Pinot Noir (Thracian Valley)",
        "points": 90,
        "price": 11,
        "ratio": 8.18
      },
      {
        "title": "Bulgariana 2012 Cabernet Sauvignon (Thracian Valley)",
        "points": 90,
        "price": 11,
        "ratio": 8.18
      },
      {
        "title": "K Cellars 2012 Cabernet Sauvignon (Thracian Valley)",
        "points": 90,
        "price": 11,
        "ratio": 8.18
      },
      {
        "title": "Domaine Boyar 2013 Reserve Cabernet Sauvignon (Thracian Valley)",
        "points": 90,
        "price": 11,
        "ratio": 8.18
      },
      {
        "title": "K Cellars 2015 Chardonnay (Thracian Valley)",
        "points": 90,
        "price": 12,
        "ratio": 7.5
      },
      {
        "title": "Bulgariana 2015 Unoaked Chardonnay (Thracian Valley)",
        "points": 90,
        "price": 12,
        "ratio": 7.5
      },
      {
        "title": "Bessa 2010 Petit Enira Merlot (Thracian Valley)",
        "points": 90,
        "price": 13,
        "ratio": 6.92
      },
      {
        "title": "Bulgariana 2015 Sauvignon Blanc (Thracian Valley)",
        "points": 90,
        "price": 13,
        "ratio": 6.92
      },
      {
        "title": "K Cellars 2011 Red (Thracian Valley)",
        "points": 90,
        "price": 14,
        "ratio": 6.43
      },
      {
        "title": "Bulgariana 2011 Pinot Noir (Thracian Valley)",
        "points": 90,
        "price": 14,
        "ratio": 6.43
      },
      {
        "title": "Bulgariana 2011 Cabernet Sauvignon-Syrah (Thracian Valley)",
        "points": 90,
        "price": 14,
        "ratio": 6.43
      },
      {
        "title": "Chateau Burgozone 2012 Cabernet Sauvignon (Danube River Plains)",
        "points": 90,
        "price": 14,
        "ratio": 6.43
      },
      {
        "title": "Chateau Burgozone 2012 Pinot Noir (Danube River Plains)",
        "points": 90,
        "price": 14,
        "ratio": 6.43
      },
      {
        "title": "Chateau Burgozone 2012 Chardonnay (Danube River Plains)",
        "points": 90,
        "price": 14,
        "ratio": 6.43
      },
      {
        "title": "Chateau Burgozone 2014 Chardonnay (Danube River Plains)",
        "points": 90,
        "price": 14,
        "ratio": 6.43
      },
      {
        "title": "Bessa 2009 Enira Red (Thracian Valley)",
        "points": 90,
        "price": 15,
        "ratio": 6
      }
    ],
    "Canada": [
      {
        "title": "Laughing Stock 2014 Portfolio Red (Okanagan Valley)",
        "points": 94,
        "price": 40,
        "ratio": 2.35
      },
      {
        "title": "Le Vieux Pin 2012 Cuvée Classique Syrah (Okanagan Valley)",
        "points": 94,
        "price": 45,
        "ratio": 2.09
      },
      {
        "title": "Cave Spring 2013 Riesling Icewine Riesling (Niagara Peninsula)",
        "points": 94,
        "price": 60,
        "ratio": 1.57
      },
      {
        "title": "Cave Spring 2015 Estate Riesling (Beamsville Bench)",
        "points": 93,
        "price": 18,
        "ratio": 5.17
      },
      {
        "title": "Cave Spring 2013 CSV Estate Bottled Riesling (Beamsville Bench)",
        "points": 93,
        "price": 25,
        "ratio": 3.72
      },
      {
        "title": "Tinhorn Creek 2013 Oldfield Series Merlot (Okanagan Valley)",
        "points": 93,
        "price": 27,
        "ratio": 3.44
      },
      {
        "title": "CedarCreek 2012 Block 5 Platinum Chardonnay (Okanagan Valley)",
        "points": 93,
        "price": 30,
        "ratio": 3.1
      },
      {
        "title": "Burrowing Owl 2014 Chardonnay (Okanagan Valley)",
        "points": 93,
        "price": 30,
        "ratio": 3.1
      },
      {
        "title": "Vanessa 2012 Syrah (Similkameen Valley)",
        "points": 93,
        "price": 34,
        "ratio": 2.74
      },
      {
        "title": "Le Vieux Pin 2012 Ava White (Okanagan Valley)",
        "points": 93,
        "price": 35,
        "ratio": 2.66
      },
      {
        "title": "CedarCreek 2012 Block 2 Platinum Pinot Noir (Okanagan Valley)",
        "points": 93,
        "price": 45,
        "ratio": 2.07
      },
      {
        "title": "Burrowing Owl 2012 Meritage (Okanagan Valley)",
        "points": 93,
        "price": 49,
        "ratio": 1.9
      },
      {
        "title": "Cave Spring 2008 Icewine Sweet Wine Riesling (Niagara Peninsula)",
        "points": 93,
        "price": 50,
        "ratio": 1.86
      },
      {
        "title": "Inniskillin 2012 Icewine Niagara Estate Riesling (Niagara Peninsula)",
        "points": 93,
        "price": 80,
        "ratio": 1.16
      },
      {
        "title": "Le Vieux Pin 2012 Equinoxe Syrah (Okanagan Valley)",
        "points": 93,
        "price": 85,
        "ratio": 1.09
      },
      {
        "title": "Peller 2008 Icewine Riesling (Niagara Peninsula)",
        "points": 93,
        "price": 90,
        "ratio": 1.03
      },
      {
        "title": "Cave Spring 2015 Riesling (Niagara Peninsula)",
        "points": 92,
        "price": 16,
        "ratio": 5.75
      },
      {
        "title": "Quails' Gate 2006 Gewürztraminer (Okanagan Valley)",
        "points": 92,
        "price": 18,
        "ratio": 5.11
      },
      {
        "title": "Township 7 2014 Naramata Estate Vineyard Gewürztraminer (Okanagan Valley)",
        "points": 92,
        "price": 18,
        "ratio": 5.11
      },
      {
        "title": "LaStella 2013 Moscato d'Osoyoos Moscato (Okanagan Valley)",
        "points": 92,
        "price": 20,
        "ratio": 4.6
      }
    ],
    "Chile": [
      {
        "title": "Valdivieso NV Caballo Loco Number Sixteen Red (Central Valley)",
        "points": 95,
        "price": 70,
        "ratio": 1.36
      },
      {
        "title": "Lapostolle 2008 Clos Apalta Red (Colchagua Valley)",
        "points": 95,
        "price": 90,
        "ratio": 1.06
      },
      {
        "title": "Santa Carolina 2012 VSC Assemblage Red (Cachapoal Valley)",
        "points": 94,
        "price": 40,
        "ratio": 2.35
      },
      {
        "title": "François Lurton 2011 Alka Carmenère (Colchagua Valley)",
        "points": 94,
        "price": 50,
        "ratio": 1.88
      },
      {
        "title": "Lapostolle 2005 Clos Apalta Red (Colchagua Valley)",
        "points": 94,
        "price": 75,
        "ratio": 1.25
      },
      {
        "title": "Emiliana 2006 Gê Red (Colchagua Valley)",
        "points": 94,
        "price": 92,
        "ratio": 1.02
      },
      {
        "title": "Santa Carolina 2011 Herencia Carmenère (Peumo)",
        "points": 94,
        "price": 100,
        "ratio": 0.94
      },
      {
        "title": "Almaviva 2013 Red (Puente Alto)",
        "points": 94,
        "price": 100,
        "ratio": 0.94
      },
      {
        "title": "Viña Requingua 2012 Potro de Piedra Family Reserve Single Vineyard Cabernet Sauvignon-Cabernet Franc (Curicó Valley)",
        "points": 93,
        "price": 30,
        "ratio": 3.1
      },
      {
        "title": "Luis Felipe Edwards 2012 LFE900 Single Vineyard Red (Colchagua Valley)",
        "points": 93,
        "price": 35,
        "ratio": 2.66
      },
      {
        "title": "Cono Sur 2013 20 Barrels Cabernet Sauvignon (Maipo Valley)",
        "points": 93,
        "price": 35,
        "ratio": 2.66
      },
      {
        "title": "Loma Larga 2012 Unfiltered Rapsodia Red (Casablanca Valley)",
        "points": 93,
        "price": 36,
        "ratio": 2.58
      },
      {
        "title": "Polkura 2012 Block g+i Syrah (Marchigue)",
        "points": 93,
        "price": 40,
        "ratio": 2.33
      },
      {
        "title": "Polkura 2013 Block G + I Syrah (Marchigue)",
        "points": 93,
        "price": 40,
        "ratio": 2.33
      },
      {
        "title": "Viña el Principal 2008 El Principal Andetelmo Cabernet Sauvignon-Carmenère (Maipo Valley)",
        "points": 93,
        "price": 54,
        "ratio": 1.72
      },
      {
        "title": "Viña el Principal 2010 Andetelmo Red (Maipo Valley)",
        "points": 93,
        "price": 54,
        "ratio": 1.72
      },
      {
        "title": "Maquis 2010 Viola Red (Colchagua Valley)",
        "points": 93,
        "price": 55,
        "ratio": 1.69
      },
      {
        "title": "Estampa 2012 LaCruz Red (Colchagua Valley)",
        "points": 93,
        "price": 56,
        "ratio": 1.66
      },
      {
        "title": "Pangea 2011 Apalta Vineyard Syrah (Colchagua Valley)",
        "points": 93,
        "price": 60,
        "ratio": 1.55
      },
      {
        "title": "Viu Manent 2005 Viu 1 Malbec (Colchagua Valley)",
        "points": 93,
        "price": 65,
        "ratio": 1.43
      }
    ],
    "Croatia": [
      {
        "title": "Trapan 2013 Terra Mare Teran (Istria)",
        "points": 91,
        "price": 33,
        "ratio": 2.76
      },
      {
        "title": "Bibich NV Ambra Red (North Dalmatia)",
        "points": 91,
        "price": 50,
        "ratio": 1.82
      },
      {
        "title": "Korta Katarina 2006 Reuben's Private Reserve Plavac Mali (Peljesac)",
        "points": 91,
        "price": 57,
        "ratio": 1.6
      },
      {
        "title": "Benvenuti 2012 Malvasia Istriana (Istria)",
        "points": 90,
        "price": 17,
        "ratio": 5.29
      },
      {
        "title": "Kozlovi? 2011 Teran (Istria)",
        "points": 90,
        "price": 18,
        "ratio": 5
      },
      {
        "title": "Kozlovi? 2012 Malvazija Malvasia (Istria)",
        "points": 90,
        "price": 18,
        "ratio": 5
      },
      {
        "title": "Trapan 2012 Ponente Malvazija Malvasia (Istria)",
        "points": 90,
        "price": 21,
        "ratio": 4.29
      },
      {
        "title": "Enjingi 2009 Barrique Zweigelt (Kutjevo)",
        "points": 90,
        "price": 25,
        "ratio": 3.6
      },
      {
        "title": "Korta Katarina 2006 Plavac Mali (Peljesac)",
        "points": 90,
        "price": 38,
        "ratio": 2.37
      },
      {
        "title": "Grgi? Vina 2008 Plavac Mali (Dinga?)",
        "points": 90,
        "price": 38,
        "ratio": 2.37
      },
      {
        "title": "Roxanich 2008 Ines in White White (Istria)",
        "points": 90,
        "price": 41,
        "ratio": 2.2
      },
      {
        "title": "Miloš 2006 Stagnum Plavac Mali (Peljesac)",
        "points": 90,
        "price": 57,
        "ratio": 1.58
      },
      {
        "title": "Bibich 2010 Riserva R6 Red (North Dalmatia)",
        "points": 89,
        "price": 20,
        "ratio": 4.45
      },
      {
        "title": "Adzic 2015 Graševina (Kutjevo)",
        "points": 89,
        "price": 20,
        "ratio": 4.45
      },
      {
        "title": "Piližota 2009 Babi? (North Dalmatia)",
        "points": 89,
        "price": 25,
        "ratio": 3.56
      },
      {
        "title": "Grgi? Vina 2010 Posip (Kor?ula)",
        "points": 89,
        "price": 28,
        "ratio": 3.18
      },
      {
        "title": "Bibich 2010 Lucica Debit (North Dalmatia)",
        "points": 89,
        "price": 35,
        "ratio": 2.54
      },
      {
        "title": "Grgi? Vina 2007 Plavac Mali (Peljesac)",
        "points": 89,
        "price": 45,
        "ratio": 1.98
      },
      {
        "title": "Coronica 2008 Gran Teran (Istria)",
        "points": 89,
        "price": 45,
        "ratio": 1.98
      },
      {
        "title": "Suha Punta 2009 Gracin Babi? (North Dalmatia)",
        "points": 89,
        "price": 60,
        "ratio": 1.48
      }
    ],
    "Cyprus": [
      {
        "title": "Loel NV Alasia Dessert Wine Red (Commandaria)",
        "points": 89,
        "price": 17,
        "ratio": 5.24
      },
      {
        "title": "Etko 2000 St. Nicholas Xinisteri (Commandaria)",
        "points": 89,
        "price": 20,
        "ratio": 4.45
      },
      {
        "title": "Tsiakkas 2014 Xynisteri (Lemesos)",
        "points": 89,
        "price": 21,
        "ratio": 4.24
      },
      {
        "title": "Chrysorroyiatissa 2010 Ayios Andronicos White (Pafos)",
        "points": 88,
        "price": 14,
        "ratio": 6.29
      },
      {
        "title": "Chrysorroyiatissa 2007 Ayios Andronicos White (Cyprus)",
        "points": 87,
        "price": 11,
        "ratio": 7.91
      },
      {
        "title": "Panayia 2008 Alina Medium Dry White Wine White (Cyprus)",
        "points": 87,
        "price": 15,
        "ratio": 5.8
      },
      {
        "title": "Kathikac 2007 Agios Onoufrios Red (Kathikas)",
        "points": 87,
        "price": 18,
        "ratio": 4.83
      },
      {
        "title": "Tsiakkas 2004 Vamvakada Red (Pitsilia Mountains)",
        "points": 85,
        "price": 16,
        "ratio": 5.31
      },
      {
        "title": "K&K; Vasilikon 2010 Xynisteri (Pafos)",
        "points": 85,
        "price": 16,
        "ratio": 5.31
      },
      {
        "title": "Chrysorroyiatissa 2008 Ayios Andronicos White (Cyprus)",
        "points": 85,
        "price": 17,
        "ratio": 5
      }
    ],
    "Czech Republic": [
      {
        "title": "Sonberk 2010 Ryzlink Rýnský (Moravia)",
        "points": 89,
        "price": 40,
        "ratio": 2.23
      },
      {
        "title": "Stapleton & Springer 2007 Roucí Red (Moravia)",
        "points": 89,
        "price": 45,
        "ratio": 1.98
      },
      {
        "title": "Vino z Czech 2013 Michlovský Riesling (Moravia)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Vino z Czech 2013 Znovín Znojmo St. Laurent (Moravia)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Vino z Czech 2009 Ludwig Cabernet Moravia (Moravia)",
        "points": 88,
        "price": 18,
        "ratio": 4.89
      },
      {
        "title": "Vino z Czech 2012 Hrabal Frankovka (Moravia)",
        "points": 88,
        "price": 24,
        "ratio": 3.67
      },
      {
        "title": "Vino z Czech 2011 Rivaner (Moravia)",
        "points": 87,
        "price": 15,
        "ratio": 5.8
      },
      {
        "title": "Vino z Czech 2013 Michlovský Sauvignon Blanc (Moravia)",
        "points": 87,
        "price": 15,
        "ratio": 5.8
      },
      {
        "title": "Vino z Czech NV Cabernet Moravia (Moravia)",
        "points": 85,
        "price": 18,
        "ratio": 4.72
      },
      {
        "title": "Vino z Czech 2011 Welschriesling (Moravia)",
        "points": 85,
        "price": 25,
        "ratio": 3.4
      },
      {
        "title": "Vino z Czech 2011 Sauvignon Blanc (Moravia)",
        "points": 84,
        "price": 16,
        "ratio": 5.25
      }
    ],
    "England": [
      {
        "title": "Chapel Down 2011 Three Graces Sparkling (England)",
        "points": 95,
        "price": 50,
        "ratio": 1.9
      },
      {
        "title": "Nyetimber 2010 Blanc de Blancs Chardonnay (England)",
        "points": 95,
        "price": 70,
        "ratio": 1.36
      },
      {
        "title": "Sugrue Pierre Ltd. 2010 The Trouble with Dreams Sparkling (England)",
        "points": 95,
        "price": 80,
        "ratio": 1.19
      },
      {
        "title": "Ridgeview Estate 2009 South Ridge Cuvée Merret Brut Sparkling (England)",
        "points": 94,
        "price": 38,
        "ratio": 2.47
      },
      {
        "title": "Wiston Estate Winery 2013 Cuvée Brut Sparkling (England)",
        "points": 94,
        "price": 40,
        "ratio": 2.35
      },
      {
        "title": "Wiston Estate Winery 2011 Rosé Sparkling (England)",
        "points": 94,
        "price": 44,
        "ratio": 2.14
      },
      {
        "title": "Wiston Estate Winery 2014 Rosé Sparkling (England)",
        "points": 94,
        "price": 44,
        "ratio": 2.14
      },
      {
        "title": "Hattingley Valley Wines 2010 Blanc de Blancs Chardonnay (England)",
        "points": 94,
        "price": 45,
        "ratio": 2.09
      },
      {
        "title": "Wiston Estate Winery 2010 Blanc de Blancs Chardonnay (England)",
        "points": 94,
        "price": 49,
        "ratio": 1.92
      },
      {
        "title": "Exton Park Vineyard 2011 Blanc de Blancs Chardonnay (England)",
        "points": 94,
        "price": 50,
        "ratio": 1.88
      },
      {
        "title": "Hoffmann & Rathbone 2011 Blanc de Blancs Chardonnay (England)",
        "points": 94,
        "price": 56,
        "ratio": 1.68
      },
      {
        "title": "Nyetimber NV Rosé Sparkling (England)",
        "points": 94,
        "price": 65,
        "ratio": 1.45
      },
      {
        "title": "Gusbourne Estate 2013 Blanc de Blancs Chardonnay (England)",
        "points": 94,
        "price": 80,
        "ratio": 1.18
      },
      {
        "title": "Bride Valley Vineyard 2014 Rosé Bella Sparkling (England)",
        "points": 94,
        "price": 85,
        "ratio": 1.11
      },
      {
        "title": "Exton Park Vineyard NV Rosé Sparkling (England)",
        "points": 93,
        "price": 43,
        "ratio": 2.16
      },
      {
        "title": "Exton Park Vineyard NV Blanc de Noirs Pinot Noir (England)",
        "points": 93,
        "price": 43,
        "ratio": 2.16
      },
      {
        "title": "Bolney Wine Estate 2014 Cuvée Rosé Brut Pinot Noir (England)",
        "points": 93,
        "price": 50,
        "ratio": 1.86
      },
      {
        "title": "Nyetimber NV Classic Cuvée Sparkling (England)",
        "points": 93,
        "price": 55,
        "ratio": 1.69
      },
      {
        "title": "Gusbourne Estate 2013 Brut Reserve Sparkling (England)",
        "points": 93,
        "price": 60,
        "ratio": 1.55
      },
      {
        "title": "Bride Valley Vineyard 2014 Brut Reserve Sparkling (England)",
        "points": 93,
        "price": 75,
        "ratio": 1.24
      }
    ],
    "France": [
      {
        "title": "Château Léoville Barton 2010  Saint-Julien",
        "points": 100,
        "price": 150,
        "ratio": 0.67
      },
      {
        "title": "Louis Roederer 2008 Cristal Vintage Brut  (Champagne)",
        "points": 100,
        "price": 250,
        "ratio": 0.4
      },
      {
        "title": "Krug 2002 Brut  (Champagne)",
        "points": 100,
        "price": 259,
        "ratio": 0.39
      },
      {
        "title": "Château Léoville Las Cases 2010  Saint-Julien",
        "points": 100,
        "price": 359,
        "ratio": 0.28
      },
      {
        "title": "Salon 2006 Le Mesnil Blanc de Blancs Brut Chardonnay (Champagne)",
        "points": 100,
        "price": 617,
        "ratio": 0.16
      },
      {
        "title": "Château Haut-Brion 2014  Pessac-Léognan",
        "points": 100,
        "price": 848,
        "ratio": 0.12
      },
      {
        "title": "Château Lafite Rothschild 2010  Pauillac",
        "points": 100,
        "price": 1500,
        "ratio": 0.07
      },
      {
        "title": "Château Cheval Blanc 2010  Saint-Émilion",
        "points": 100,
        "price": 1500,
        "ratio": 0.07
      },
      {
        "title": "Domaine Huët 2009 Cuvée Constance 500ml  (Vouvray)",
        "points": 99,
        "price": 159,
        "ratio": 0.62
      },
      {
        "title": "Domaine Leflaive 2010  Bâtard-Montrachet",
        "points": 99,
        "price": 560,
        "ratio": 0.18
      },
      {
        "title": "Krug 2002 Clos du Mesnil Brut Blanc de Blancs Chardonnay (Champagne)",
        "points": 99,
        "price": 800,
        "ratio": 0.12
      },
      {
        "title": "Château Climens 2014  Barsac",
        "points": 98,
        "price": 70,
        "ratio": 1.4
      },
      {
        "title": "Château Léoville Poyferré 2010  Saint-Julien",
        "points": 98,
        "price": 92,
        "ratio": 1.07
      },
      {
        "title": "Louis Jadot 2005  Charmes-Chambertin",
        "points": 98,
        "price": 134,
        "ratio": 0.73
      },
      {
        "title": "Louis Jadot 2005  Clos de la Roche",
        "points": 98,
        "price": 138,
        "ratio": 0.71
      },
      {
        "title": "M. Chapoutier 1999 Le Méal Ermitage  (Hermitage)",
        "points": 98,
        "price": 150,
        "ratio": 0.65
      },
      {
        "title": "Pol Roger 2002 Cuvée Sir Winston Churchill Brut  (Champagne)",
        "points": 98,
        "price": 305,
        "ratio": 0.32
      },
      {
        "title": "Clos de Tart 2005  Clos de Tart",
        "points": 98,
        "price": 319,
        "ratio": 0.31
      },
      {
        "title": "Château Léoville Las Cases 2009  Saint-Julien",
        "points": 98,
        "price": 360,
        "ratio": 0.27
      },
      {
        "title": "Louis Jadot 2014  Bâtard-Montrachet",
        "points": 98,
        "price": 367,
        "ratio": 0.27
      }
    ],
    "Georgia": [
      {
        "title": "Alaverdi Monastery Cellar 2010 Qvevri Traditional Kakhuri Dry Unfiltered Amber Wine Rkatsiteli (Kakheti)",
        "points": 92,
        "price": 25,
        "ratio": 3.68
      },
      {
        "title": "Kindzmarauli Marani 2013 Saperavi (Kakheti)",
        "points": 90,
        "price": 15,
        "ratio": 6
      },
      {
        "title": "Tbilvino 2014 Georgian Valleys Saperavi (Kakheti)",
        "points": 90,
        "price": 15,
        "ratio": 6
      },
      {
        "title": "Dilao 2015 Saperavi (Kakheti)",
        "points": 90,
        "price": 15,
        "ratio": 6
      },
      {
        "title": "Teliani Valley 2013 Mukuzani Saperavi (Kakheti)",
        "points": 90,
        "price": 16,
        "ratio": 5.63
      },
      {
        "title": "Kindzmarauli Marani 2013 Kakhetian Royal White (Kakheti)",
        "points": 90,
        "price": 17,
        "ratio": 5.29
      },
      {
        "title": "Teliani Valley 2013 Kindzmarauli Saperavi (Kakheti)",
        "points": 90,
        "price": 17,
        "ratio": 5.29
      },
      {
        "title": "Kindzmarauli Marani 2014 Kindzmarauli Original Saperavi (Kakheti)",
        "points": 90,
        "price": 17,
        "ratio": 5.29
      },
      {
        "title": "Pheasant's Tears 2009 Dry Unfiltered Amber Wine Rkatsiteli (Kakheti)",
        "points": 90,
        "price": 18,
        "ratio": 5
      },
      {
        "title": "Marani 2013 Kondoli Single Vineyard Saperavi (Kakheti)",
        "points": 90,
        "price": 18,
        "ratio": 5
      },
      {
        "title": "Marani 2013 Kondoli Single Vineyard Saperavi-Merlot (Kakheti)",
        "points": 90,
        "price": 18,
        "ratio": 5
      },
      {
        "title": "Teliani Valley 2015 Semi-Sweet Khvanchkara Red (Georgia)",
        "points": 90,
        "price": 20,
        "ratio": 4.5
      },
      {
        "title": "Antadze 2010 Kvevri Wine Mtsvane (Kakheti)",
        "points": 90,
        "price": 25,
        "ratio": 3.6
      },
      {
        "title": "Antadze 2014 Kvevri Wine Saperavi (Kakheti)",
        "points": 90,
        "price": 28,
        "ratio": 3.21
      },
      {
        "title": "Orgo 2015 Saperavi (Kakheti)",
        "points": 90,
        "price": 28,
        "ratio": 3.21
      },
      {
        "title": "Shalauri Cellars 2013 Saperavi (Kakheti)",
        "points": 90,
        "price": 34,
        "ratio": 2.65
      },
      {
        "title": "Schuchmann Wines 2014 Saperavi (Kakheti)",
        "points": 89,
        "price": 13,
        "ratio": 6.85
      },
      {
        "title": "Marani 2014 Kondoli Single Vineyard Mtsvane-Kisi White (Kakheti)",
        "points": 89,
        "price": 14,
        "ratio": 6.36
      },
      {
        "title": "Shumi Winery 2014 Saperavi (Kakheti)",
        "points": 89,
        "price": 15,
        "ratio": 5.93
      },
      {
        "title": "Dergi 2007 Qvevri Dry Unfiltered White Wine Rkatsiteli (Kakheti)",
        "points": 89,
        "price": 18,
        "ratio": 4.94
      }
    ],
    "Germany": [
      {
        "title": "Robert Weil 2015 Kiedrich Gräfenberg Trockenbeerenauslese Riesling (Rheingau)",
        "points": 98,
        "price": 775,
        "ratio": 0.13
      },
      {
        "title": "Geh. Rat Dr. von Bassermann-Jordan 2008 Deidesheimer Hohenmorgen Trockenbeerenauslese - 375 ml Riesling (Pfalz)",
        "points": 97,
        "price": 245,
        "ratio": 0.4
      },
      {
        "title": "Dr. Pauly Bergweiler 2007 Bernkasteler Badstube Trockenbeerenauslese Riesling (Mosel-Saar-Ruwer)",
        "points": 97,
        "price": 250,
        "ratio": 0.39
      },
      {
        "title": "S.A. Prüm 2006 Wehlener Sonnenuhr Trockenbeerenauslese Erste Lage Riesling (Mosel)",
        "points": 97,
        "price": 440,
        "ratio": 0.22
      },
      {
        "title": "Schloss Johannisberger 2009 Trockenbeerenauslese Riesling (Rheingau)",
        "points": 97,
        "price": 445,
        "ratio": 0.22
      },
      {
        "title": "Schloss Johannisberger 2006 Trockenbeerenauslese Goldlack Riesling (Rheingau)",
        "points": 97,
        "price": 486,
        "ratio": 0.2
      },
      {
        "title": "Robert Weil 2009 Kiedrich Gräfenberg Trockenbeerenauslese Riesling (Rheingau)",
        "points": 97,
        "price": 612,
        "ratio": 0.16
      },
      {
        "title": "Robert Weil 2014 Kiedrich Gräfenberg Trockenbeerenauslese Riesling (Rheingau)",
        "points": 97,
        "price": 775,
        "ratio": 0.13
      },
      {
        "title": "Dr. Loosen 2006 Wehlener Sonnenuhr Trockenbeerenauslese Goldkap Riesling (Mosel-Saar-Ruwer)",
        "points": 96,
        "price": 49,
        "ratio": 1.96
      },
      {
        "title": "Karl Erbes 2006 Ürziger Würzgarten Auslese * * * Goldkap Riesling (Mosel-Saar-Ruwer)",
        "points": 96,
        "price": 53,
        "ratio": 1.81
      },
      {
        "title": "Dr. Heidemanns-Bergweiler 2009 Bernkasteler Johannisbrünnchen Eiswein Riesling (Mosel)",
        "points": 96,
        "price": 80,
        "ratio": 1.2
      },
      {
        "title": "Dr. Pauly Bergweiler 2009 Bernkasteler Johannisbrünnchen Eiswein Riesling (Mosel)",
        "points": 96,
        "price": 95,
        "ratio": 1.01
      },
      {
        "title": "Thörle 2011 Saulheimer Hölle Trockenbeerenauslese Riesling (Rheinhessen)",
        "points": 96,
        "price": 150,
        "ratio": 0.64
      },
      {
        "title": "Dr. H. Thanisch (Erben Müller-Burggraef) 2006 Berncasteler Doctor Beerenauslese Goldkap Riesling (Mosel-Saar-Ruwer)",
        "points": 96,
        "price": 160,
        "ratio": 0.6
      },
      {
        "title": "St. Urbans-Hof 2008 Leiwener Laurentiuslay Beerenauslese Riesling (Mosel-Saar-Ruwer)",
        "points": 96,
        "price": 175,
        "ratio": 0.55
      },
      {
        "title": "S.A. Prüm 2006 Wehlener Sonnenuhr Beerenauslese Erste Lage Riesling (Mosel)",
        "points": 96,
        "price": 200,
        "ratio": 0.48
      },
      {
        "title": "Castell 2012 Casteller Kugelspiel Eiswein Silvaner (Franken)",
        "points": 96,
        "price": 220,
        "ratio": 0.44
      },
      {
        "title": "Maximin Grünhäuser 2009 Herrenberg Eiswein Riesling (Mosel)",
        "points": 96,
        "price": 228,
        "ratio": 0.42
      },
      {
        "title": "Domdechant Werner 2009 Hochheimer Domdechaney Beerenauslese Riesling (Rheingau)",
        "points": 96,
        "price": 271,
        "ratio": 0.35
      },
      {
        "title": "Domdechant Werner 2015 Hochheimer Domdechaney Trockenbeerenauslese Grosse Lage Riesling (Rheingau)",
        "points": 96,
        "price": 316,
        "ratio": 0.3
      }
    ],
    "Greece": [
      {
        "title": "Dionysos 2014 Moschofilero (Mantinia)",
        "points": 93,
        "price": 16,
        "ratio": 5.81
      },
      {
        "title": "Domaine Sigalas 2014 Kavalieros Single Vineyard Assyrtiko (Santorini)",
        "points": 93,
        "price": 35,
        "ratio": 2.66
      },
      {
        "title": "Lyrarakis 2014 Kotsifali (Crete)",
        "points": 92,
        "price": 13,
        "ratio": 7.08
      },
      {
        "title": "Boutari 2016 Moschofilero (Mantinia)",
        "points": 92,
        "price": 17,
        "ratio": 5.41
      },
      {
        "title": "Nasiakos 2013 Agiorgitiko (Nemea)",
        "points": 92,
        "price": 19,
        "ratio": 4.84
      },
      {
        "title": "Lantides 2014 Ancient Varietal Moschofilero (Peloponnese)",
        "points": 92,
        "price": 20,
        "ratio": 4.6
      },
      {
        "title": "Ktima Pavlidis 2015 Thema White (Drama)",
        "points": 92,
        "price": 20,
        "ratio": 4.6
      },
      {
        "title": "Kechris 2015 Tear of the Pine Assyrtico (Retsina)",
        "points": 92,
        "price": 20,
        "ratio": 4.6
      },
      {
        "title": "Boutari 2015 Assyrtico (Santorini)",
        "points": 92,
        "price": 21,
        "ratio": 4.38
      },
      {
        "title": "Domaine Costa Lazaridi 2012 Amethystos Red (Drama)",
        "points": 92,
        "price": 22,
        "ratio": 4.18
      },
      {
        "title": "Gerovassiliou 2013 Single Vineyard Malagousia (Epanomi)",
        "points": 92,
        "price": 23,
        "ratio": 4
      },
      {
        "title": "Gerovassiliou 2015 Single Vineyard Malagousia (Epanomi)",
        "points": 92,
        "price": 23,
        "ratio": 4
      },
      {
        "title": "Domaine Sigalas 2010 Assyrtico (Santorini)",
        "points": 92,
        "price": 24,
        "ratio": 3.83
      },
      {
        "title": "Tselepos 2014 Assyrtico (Santorini)",
        "points": 92,
        "price": 25,
        "ratio": 3.68
      },
      {
        "title": "Tselepos 2015 Canava Chrissou Assyrtiko (Santorini)",
        "points": 92,
        "price": 25,
        "ratio": 3.68
      },
      {
        "title": "Kir-Yianni 2012 Ramnista Single Vineyard Xinomavro (Naoussa)",
        "points": 92,
        "price": 27,
        "ratio": 3.41
      },
      {
        "title": "Alpha Estate 2008 Alpha One Unfiltered Merlot (Florina)",
        "points": 92,
        "price": 59,
        "ratio": 1.56
      },
      {
        "title": "Hatzimichalis 2015 Assyrtiko (Atalanti Valley)",
        "points": 91,
        "price": 12,
        "ratio": 7.58
      },
      {
        "title": "Nasiakos 2015 Moschofilero (Mantinia)",
        "points": 91,
        "price": 16,
        "ratio": 5.69
      },
      {
        "title": "Tselepos 2015 Moschofilero (Mantinia)",
        "points": 91,
        "price": 17,
        "ratio": 5.35
      }
    ],
    "Hungary": [
      {
        "title": "Royal Tokaji 2013 6 Puttonyos Aszú Gold Label  (Tokaji)",
        "points": 97,
        "price": 80,
        "ratio": 1.21
      },
      {
        "title": "Royal Tokaji 2013 5 Puttonyos Aszú Red Label  (Tokaji)",
        "points": 96,
        "price": 54,
        "ratio": 1.78
      },
      {
        "title": "Dobogó 2007 Aszú 6 Puttonyos  (Tokaji)",
        "points": 96,
        "price": 125,
        "ratio": 0.77
      },
      {
        "title": "Oremus 2005 Eszencia  (Tokaji)",
        "points": 96,
        "price": 320,
        "ratio": 0.3
      },
      {
        "title": "Chateau Dereszla 2009 Aszú 5 Puttonyos  (Tokaji)",
        "points": 95,
        "price": 43,
        "ratio": 2.21
      },
      {
        "title": "Royal Tokaji 2016 Late Harvest  (Tokaji)",
        "points": 94,
        "price": 21,
        "ratio": 4.48
      },
      {
        "title": "Royal Tokaji 1999 Mézes Mály Aszú 6 Puttonyos  (Tokaji)",
        "points": 94,
        "price": 175,
        "ratio": 0.54
      },
      {
        "title": "Royal Tokaji 2003 Essencia  (Tokaji)",
        "points": 94,
        "price": 764,
        "ratio": 0.12
      },
      {
        "title": "Dobogó 2011 Szent Tamás Betsek Vineyards  (Tokaji)",
        "points": 93,
        "price": 25,
        "ratio": 3.72
      },
      {
        "title": "Patricius 2012 Katinka Noble Late Harvest  (Tokaji)",
        "points": 93,
        "price": 25,
        "ratio": 3.72
      },
      {
        "title": "Disznókö 2012 Késöi Szüret Late Harvest  (Tokaji)",
        "points": 93,
        "price": 26,
        "ratio": 3.58
      },
      {
        "title": "Oremus 2014 Late Harvest  (Tokaji)",
        "points": 93,
        "price": 32,
        "ratio": 2.91
      },
      {
        "title": "Béres 2007 Aszú 5 Puttonyos  (Tokaji)",
        "points": 93,
        "price": 50,
        "ratio": 1.86
      },
      {
        "title": "Royal Tokaji 2011  Tokaji",
        "points": 92,
        "price": 16,
        "ratio": 5.75
      },
      {
        "title": "Dobogó 2012 Mylitta Furmint (Tokaj)",
        "points": 92,
        "price": 40,
        "ratio": 2.3
      },
      {
        "title": "Samuel Tinon 2007 Dry Tokaji Szamorodni Furmint (Tokaj)",
        "points": 92,
        "price": 46,
        "ratio": 2
      },
      {
        "title": "Tokaj Classic 2000 Aszú 6 Puttonyos - 500 ml Tokaji (Tokaj)",
        "points": 92,
        "price": 59,
        "ratio": 1.56
      },
      {
        "title": "Oremus 2006 Aszú 5 Puttonyos  (Tokaji)",
        "points": 92,
        "price": 70,
        "ratio": 1.31
      },
      {
        "title": "Tokaj Classic 1999 Aszú 6 Puttonyos - 500 ml Tokaji (Tokaj)",
        "points": 92,
        "price": 80,
        "ratio": 1.15
      },
      {
        "title": "Royal Tokaji 2007 Betsek Aszú 6 Puttonyos  (Tokaji)",
        "points": 92,
        "price": 118,
        "ratio": 0.78
      }
    ],
    "India": [
      {
        "title": "Sula 2011 Dindori Reserve Shiraz (Nashik)",
        "points": 93,
        "price": 20,
        "ratio": 4.65
      },
      {
        "title": "Sula 2014 Dindori Reserve Shiraz (Nashik)",
        "points": 92,
        "price": 19,
        "ratio": 4.84
      },
      {
        "title": "Sula 2012 Estate Bottled Shiraz (Nashik)",
        "points": 91,
        "price": 12,
        "ratio": 7.58
      },
      {
        "title": "Sula 2013 Chenin Blanc (Nashik)",
        "points": 90,
        "price": 10,
        "ratio": 9
      },
      {
        "title": "Sula 2013 Sauvignon Blanc (Nashik)",
        "points": 90,
        "price": 12,
        "ratio": 7.5
      },
      {
        "title": "Sula 2015 Estate Bottled Shiraz (Nashik)",
        "points": 90,
        "price": 13,
        "ratio": 6.92
      },
      {
        "title": "Sula 2015 Sauvignon Blanc (Nashik)",
        "points": 89,
        "price": 12,
        "ratio": 7.42
      },
      {
        "title": "Sula 2015 Chenin Blanc (Nashik)",
        "points": 87,
        "price": 12,
        "ratio": 7.25
      }
    ],
    "Israel": [
      {
        "title": "Recanati 2011 Reserve David Vineyard Cabernet Sauvignon (Galilee)",
        "points": 94,
        "price": 25,
        "ratio": 3.76
      },
      {
        "title": "Jerusalem Wineries 2013 3400 Premium Shiraz (Judean Hills)",
        "points": 93,
        "price": 17,
        "ratio": 5.47
      },
      {
        "title": "Recanati 2014 Shiraz (Galilee)",
        "points": 93,
        "price": 17,
        "ratio": 5.47
      },
      {
        "title": "Dalton 2012 Oak Aged Petite Sirah (Samson)",
        "points": 93,
        "price": 25,
        "ratio": 3.72
      },
      {
        "title": "Dalton 2014 Alma Red (Galilee)",
        "points": 93,
        "price": 25,
        "ratio": 3.72
      },
      {
        "title": "Jezreel 2014 Adumim Dry Red (Israel)",
        "points": 93,
        "price": 30,
        "ratio": 3.1
      },
      {
        "title": "Golan Heights Winery 2011 Yarden Cabernet Sauvignon (Galilee)",
        "points": 93,
        "price": 33,
        "ratio": 2.82
      },
      {
        "title": "Shiloh Winery 2010 Legend Red (Judean Hills)",
        "points": 93,
        "price": 40,
        "ratio": 2.33
      },
      {
        "title": "Recanati 2014 Reserve Syrah (Galilee)",
        "points": 93,
        "price": 40,
        "ratio": 2.33
      },
      {
        "title": "Recanati 2012 Reserve Red (Galilee)",
        "points": 93,
        "price": 56,
        "ratio": 1.66
      },
      {
        "title": "Domaine du Castel 2012 Grand Vin Red (Judean Hills)",
        "points": 93,
        "price": 80,
        "ratio": 1.16
      },
      {
        "title": "Jerusalem Wineries 2013 3400 Premium Cabernet Sauvignon (Judean Hills)",
        "points": 92,
        "price": 17,
        "ratio": 5.41
      },
      {
        "title": "Recanati 2013 Merlot (Galilee)",
        "points": 92,
        "price": 17,
        "ratio": 5.41
      },
      {
        "title": "Recanati 2015 Cabernet Sauvignon (Upper Galilee)",
        "points": 92,
        "price": 17,
        "ratio": 5.41
      },
      {
        "title": "Recanati 2015 Shiraz (Upper Galilee)",
        "points": 92,
        "price": 17,
        "ratio": 5.41
      },
      {
        "title": "Dalton 2013 Alma Scarlet Red (Galilee)",
        "points": 92,
        "price": 22,
        "ratio": 4.18
      },
      {
        "title": "Jacques Capsouto 2014 Cotes de Galilee Village Cuvee Samuel Rouge Red (Galilee)",
        "points": 92,
        "price": 25,
        "ratio": 3.68
      },
      {
        "title": "Jerusalem Wineries 2012 4990 Reserve Cabernet Sauvignon (Judean Hills)",
        "points": 92,
        "price": 28,
        "ratio": 3.29
      },
      {
        "title": "Dalton 2012 Reserve Shiraz (Galilee)",
        "points": 92,
        "price": 28,
        "ratio": 3.29
      },
      {
        "title": "Golan Heights Winery 2010 Yarden Merlot (Galilee)",
        "points": 92,
        "price": 29,
        "ratio": 3.17
      }
    ],
    "Italy": [
      {
        "title": "Avignonesi 1995 Occhio di Pernice  (Vin Santo di Montepulciano)",
        "points": 100,
        "price": 210,
        "ratio": 0.48
      },
      {
        "title": "Casanova di Neri 2007 Cerretalto  (Brunello di Montalcino)",
        "points": 100,
        "price": 270,
        "ratio": 0.37
      },
      {
        "title": "Tenuta dell'Ornellaia 2007 Masseto Merlot (Toscana)",
        "points": 100,
        "price": 460,
        "ratio": 0.22
      },
      {
        "title": "Biondi Santi 2010 Riserva  (Brunello di Montalcino)",
        "points": 100,
        "price": 550,
        "ratio": 0.18
      },
      {
        "title": "Mascarello Giuseppe e Figlio 2010 Monprivato  (Barolo)",
        "points": 99,
        "price": 175,
        "ratio": 0.57
      },
      {
        "title": "Il Marroneto 2012 Madonna delle Grazie  (Brunello di Montalcino)",
        "points": 99,
        "price": 200,
        "ratio": 0.5
      },
      {
        "title": "Tenuta San Guido 2012 Sassicaia  (Bolgheri Sassicaia)",
        "points": 99,
        "price": 235,
        "ratio": 0.42
      },
      {
        "title": "Avignonesi 1997 Occhio di Pernice  (Vin Santo di Montepulciano)",
        "points": 99,
        "price": 237,
        "ratio": 0.42
      },
      {
        "title": "Tenuta dell'Ornellaia 2004 Masseto Merlot (Toscana)",
        "points": 99,
        "price": 250,
        "ratio": 0.4
      },
      {
        "title": "Le Macchiole 2007 Messorio Merlot (Toscana)",
        "points": 99,
        "price": 320,
        "ratio": 0.31
      },
      {
        "title": "Gaja 2007 Sorì Tildìn Nebbiolo (Langhe)",
        "points": 99,
        "price": 440,
        "ratio": 0.23
      },
      {
        "title": "Gaja 2007 Sorì San Lorenzo Nebbiolo (Langhe)",
        "points": 99,
        "price": 440,
        "ratio": 0.23
      },
      {
        "title": "Mascarello Giuseppe e Figlio 2008 Cà d'Morissio Riserva  (Barolo)",
        "points": 99,
        "price": 595,
        "ratio": 0.17
      },
      {
        "title": "Brezza 2013 Cannubi  (Barolo)",
        "points": 98,
        "price": 60,
        "ratio": 1.63
      },
      {
        "title": "Comm. G. B. Burlotto 2013 Monvigliero  (Barolo)",
        "points": 98,
        "price": 70,
        "ratio": 1.4
      },
      {
        "title": "Brovia 2013 Garblèt Suè  (Barolo)",
        "points": 98,
        "price": 83,
        "ratio": 1.18
      },
      {
        "title": "Conti Costanti 2012  Brunello di Montalcino",
        "points": 98,
        "price": 95,
        "ratio": 1.03
      },
      {
        "title": "Marchesi Antinori 2008 Guado al Tasso  (Bolgheri Superiore)",
        "points": 98,
        "price": 102,
        "ratio": 0.96
      },
      {
        "title": "Baricci 2010 Nello Riserva  (Brunello di Montalcino)",
        "points": 98,
        "price": 120,
        "ratio": 0.82
      },
      {
        "title": "Massolino 2011 Vigna Rionda Riserva  (Barolo)",
        "points": 98,
        "price": 151,
        "ratio": 0.65
      }
    ],
    "Lebanon": [
      {
        "title": "Ixsir 2009 Altitudes Red (Lebanon)",
        "points": 91,
        "price": 20,
        "ratio": 4.55
      },
      {
        "title": "Château Ksara 2011 Reserve du Couvent Red (Bekaa Valley)",
        "points": 91,
        "price": 22,
        "ratio": 4.14
      },
      {
        "title": "Massaya 2012 Terrasses de Baalbeck Red (Bekaa Valley)",
        "points": 91,
        "price": 27,
        "ratio": 3.37
      },
      {
        "title": "Ixsir 2009 Grande Réserve Syrah-Cabernet Sauvignon (Lebanon)",
        "points": 91,
        "price": 30,
        "ratio": 3.03
      },
      {
        "title": "Ixsir 2010 Grande Reserve Syrah-Cabernet Sauvignon (Lebanon)",
        "points": 91,
        "price": 40,
        "ratio": 2.28
      },
      {
        "title": "Château Musar 2000 Gaston Hochar Red (Bekaa Valley)",
        "points": 91,
        "price": 63,
        "ratio": 1.44
      },
      {
        "title": "Ixsir 2011 El Red (Lebanon)",
        "points": 91,
        "price": 75,
        "ratio": 1.21
      },
      {
        "title": "Ixsir 2010 Altitudes Red (Lebanon)",
        "points": 90,
        "price": 25,
        "ratio": 3.6
      },
      {
        "title": "Ixsir 2010 El Red (Lebanon)",
        "points": 90,
        "price": 75,
        "ratio": 1.2
      },
      {
        "title": "Massaya 2013 Le Colombier Red (Bekaa Valley)",
        "points": 89,
        "price": 13,
        "ratio": 6.85
      },
      {
        "title": "Château Musar 2011 Musar Jeune White (Bekaa Valley)",
        "points": 89,
        "price": 22,
        "ratio": 4.05
      },
      {
        "title": "Ixsir 2013 Altitudes White (Lebanon)",
        "points": 89,
        "price": 25,
        "ratio": 3.56
      },
      {
        "title": "Ixsir 2014 Altitudes Rosé (Lebanon)",
        "points": 89,
        "price": 25,
        "ratio": 3.56
      },
      {
        "title": "Massaya 2014 Rosé (Bekaa Valley)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Massaya 2014 White (Bekaa Valley)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Ixsir 2012 Altitudes Rosé (Lebanon)",
        "points": 88,
        "price": 20,
        "ratio": 4.4
      },
      {
        "title": "Château Musar 2010 Musar Jeune Red (Bekaa Valley)",
        "points": 88,
        "price": 22,
        "ratio": 4
      },
      {
        "title": "Ixsir 2014 Altitudes White (Lebanon)",
        "points": 88,
        "price": 25,
        "ratio": 3.52
      },
      {
        "title": "Château Ksara 2011 Cuvée du Pape Chardonnay (Lebanon)",
        "points": 88,
        "price": 30,
        "ratio": 2.93
      },
      {
        "title": "Château Musar 2004 Gaston Hochar Red (Bekaa Valley)",
        "points": 88,
        "price": 51,
        "ratio": 1.73
      }
    ],
    "Luxembourg": [
      {
        "title": "Domaines Vinsmoselle 2014 Bech-Kleinmacher Naumberg Grand Premier Cru Auxerrois (Moselle Luxembourgeoise)",
        "points": 90,
        "price": 22,
        "ratio": 4.09
      },
      {
        "title": "Domaines Vinsmoselle NV Pinot Luxembourg White (Moselle Luxembourgeoise)",
        "points": 89,
        "price": 16,
        "ratio": 5.56
      },
      {
        "title": "Poll-Fabaire NV Cuvée Brut Crémant de Luxembourg Sparkling (Moselle Luxembourgeoise)",
        "points": 89,
        "price": 26,
        "ratio": 3.42
      },
      {
        "title": "Domaines Vinsmoselle 2015 Wormeldange Weinbour Grand Premier Cru Riesling (Moselle Luxembourgeoise)",
        "points": 88,
        "price": 23,
        "ratio": 3.83
      },
      {
        "title": "Poll-Fabaire NV Poll on Ice Crémant de Luxembourg Demi-Sec Sparkling (Moselle Luxembourgeoise)",
        "points": 88,
        "price": 30,
        "ratio": 2.93
      }
    ],
    "Macedonia": [
      {
        "title": "Stobi 2013 Vranec (Tikves)",
        "points": 89,
        "price": 15,
        "ratio": 5.93
      },
      {
        "title": "Stobi 2011 Veritas Vranec (Tikves)",
        "points": 89,
        "price": 20,
        "ratio": 4.45
      },
      {
        "title": "Bovin 2007 Sauvignon (Tikves)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Stobi 2014 Rkatsiteli (Tikves)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Stobi 2014 Žilavka (Tikves)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Bovin 2008 Chardonnay (Tikves)",
        "points": 87,
        "price": 15,
        "ratio": 5.8
      },
      {
        "title": "Stobi 2011 Macedon Pinot Noir (Tikves)",
        "points": 87,
        "price": 15,
        "ratio": 5.8
      },
      {
        "title": "Bovin 2006 Alexandar Red (Tikves)",
        "points": 86,
        "price": 16,
        "ratio": 5.38
      },
      {
        "title": "Bovin 2007 Symphony White (Tikves)",
        "points": 85,
        "price": 15,
        "ratio": 5.67
      },
      {
        "title": "Macedon 2010 Pinot Noir (Tikves)",
        "points": 84,
        "price": 15,
        "ratio": 5.6
      },
      {
        "title": "Bovin 2007 Vranec (Tikves)",
        "points": 84,
        "price": 16,
        "ratio": 5.25
      }
    ],
    "Mexico": [
      {
        "title": "Viñas de Garza 2007 Amado IV Red (Valle de Guadalupe)",
        "points": 92,
        "price": 54,
        "ratio": 1.7
      },
      {
        "title": "Vinisterra 2007 Pedregal Syrah-Mourvèdre (San Antonio de las Minas Valley)",
        "points": 91,
        "price": 45,
        "ratio": 2.02
      },
      {
        "title": "El Sombrero 2009 Red (Valle de Guadalupe)",
        "points": 91,
        "price": 108,
        "ratio": 0.84
      },
      {
        "title": "Monte Xanic 2010 Gran Ricardo Red (Valle de Guadalupe)",
        "points": 90,
        "price": 56,
        "ratio": 1.61
      },
      {
        "title": "Tres Valles 2010 Kuwal Red (Valle de Guadalupe)",
        "points": 89,
        "price": 31,
        "ratio": 2.87
      },
      {
        "title": "L.A. Cetto 1996 Private Reserve Nebbiolo (Valle de Guadalupe)",
        "points": 88,
        "price": 18,
        "ratio": 4.89
      },
      {
        "title": "L.A. Cetto 2007 Private Reserve Barrel Aged Nebbiolo (Valle de Guadalupe)",
        "points": 88,
        "price": 20,
        "ratio": 4.4
      },
      {
        "title": "Viñas Pijoan 2009 Doménica Red (Valle de Guadalupe)",
        "points": 88,
        "price": 22,
        "ratio": 4
      },
      {
        "title": "Cava Maciel 2012 Vino de Luna Chardonnay (Valle de Guadalupe)",
        "points": 88,
        "price": 22,
        "ratio": 4
      },
      {
        "title": "Rincón de Guadalupe 2008 Brisas del Sur Red (San Vicente)",
        "points": 88,
        "price": 26,
        "ratio": 3.38
      },
      {
        "title": "Cava Aragon 2011 Madera 5 Nebbiolo (San Vicente)",
        "points": 88,
        "price": 27,
        "ratio": 3.26
      },
      {
        "title": "Paralelo 2007 Txtura 1 Red (Valle de Guadalupe)",
        "points": 88,
        "price": 30,
        "ratio": 2.93
      },
      {
        "title": "Vinicola Fraternidad 2011 Boceto Red (Valle de Guadalupe)",
        "points": 88,
        "price": 39,
        "ratio": 2.26
      },
      {
        "title": "La Lomita 2009 Singular Red (Valle de Guadalupe)",
        "points": 88,
        "price": 40,
        "ratio": 2.2
      },
      {
        "title": "Monte Xanic 2011 Chenin-Colombard White (Valle de Guadalupe)",
        "points": 87,
        "price": 12,
        "ratio": 7.25
      },
      {
        "title": "Monte Xanic 2012 Viña Kristel Sauvignon Blanc (Valle de Guadalupe)",
        "points": 87,
        "price": 15,
        "ratio": 5.8
      },
      {
        "title": "Monte Xanic 2012 Chenin-Colombard Chenin Blanc (Valle de Guadalupe)",
        "points": 87,
        "price": 16,
        "ratio": 5.44
      },
      {
        "title": "L.A. Cetto 1996 Private Reserve Cabernet Sauvignon (Valle de Guadalupe)",
        "points": 87,
        "price": 18,
        "ratio": 4.83
      },
      {
        "title": "Möebius 2011 Endemico Muscatel (Valle de Guadalupe)",
        "points": 87,
        "price": 19,
        "ratio": 4.58
      },
      {
        "title": "L.A. Cetto 2008 Private Reserve Petite Sirah (Valle de Guadalupe)",
        "points": 87,
        "price": 20,
        "ratio": 4.35
      }
    ],
    "Moldova": [
      {
        "title": "Purcari 2013 Freedom Blend Red (Moldova)",
        "points": 91,
        "price": 25,
        "ratio": 3.64
      },
      {
        "title": "Purcari 2015 1827 Cabernet Sauvignon (Moldova)",
        "points": 90,
        "price": 15,
        "ratio": 6
      },
      {
        "title": "Purcari 2015 1827 Rara Neagra (Moldova)",
        "points": 90,
        "price": 16,
        "ratio": 5.63
      },
      {
        "title": "Purcari 2015 Rose de Purcari Rosé (Moldova)",
        "points": 90,
        "price": 16,
        "ratio": 5.63
      },
      {
        "title": "Lion-Gri 2009 Chateau d'Or Ice Wine Riesling (Moldova)",
        "points": 90,
        "price": 23,
        "ratio": 3.91
      },
      {
        "title": "Purcari 2012 Negru de Purcari Red (Moldova)",
        "points": 90,
        "price": 31,
        "ratio": 2.9
      },
      {
        "title": "Purcari 2012 Rosu de Purcari Red (Moldova)",
        "points": 90,
        "price": 32,
        "ratio": 2.81
      },
      {
        "title": "Purcari 2013 Alb de Purcari White (Moldova)",
        "points": 90,
        "price": 32,
        "ratio": 2.81
      },
      {
        "title": "Purcari 2010 Negru de Purcari Red (Moldova)",
        "points": 90,
        "price": 38,
        "ratio": 2.37
      },
      {
        "title": "Purcari 2010 Rosu de Purcari Red (Moldova)",
        "points": 90,
        "price": 38,
        "ratio": 2.37
      },
      {
        "title": "Purcari 2012 Alb de Purcari White (Moldova)",
        "points": 90,
        "price": 38,
        "ratio": 2.37
      },
      {
        "title": "Lion-Gri 2013 Naturalles Muscat (Moldova)",
        "points": 89,
        "price": 10,
        "ratio": 8.9
      },
      {
        "title": "Château Vartely 2013 D'Or Matur Traminer (Moldova)",
        "points": 89,
        "price": 15,
        "ratio": 5.93
      },
      {
        "title": "Purcari 2015 1827 Sauvignon Blanc (Moldova)",
        "points": 89,
        "price": 15,
        "ratio": 5.93
      },
      {
        "title": "Château Vartely 2015 Indvido Red (Moldova)",
        "points": 89,
        "price": 15,
        "ratio": 5.93
      },
      {
        "title": "Purcari 2015 1827 Pinot Grigio (Moldova)",
        "points": 89,
        "price": 15,
        "ratio": 5.93
      },
      {
        "title": "Purcari 2014 Vinohora Rara Neagra-Malbec Red (Moldova)",
        "points": 89,
        "price": 16,
        "ratio": 5.56
      },
      {
        "title": "Purcari 2014 Rara Neagra (Moldova)",
        "points": 89,
        "price": 19,
        "ratio": 4.68
      },
      {
        "title": "Château Vartely 2011 Taraboste Rezerva Cabernet Sauvignon (Moldova)",
        "points": 89,
        "price": 25,
        "ratio": 3.56
      },
      {
        "title": "Equinox Estate 2012 5 Elemente Red (Moldova)",
        "points": 89,
        "price": 39,
        "ratio": 2.28
      }
    ],
    "Morocco": [
      {
        "title": "Ouled Thaleb 2012 Médaillon Red (Zenata)",
        "points": 93,
        "price": 18,
        "ratio": 5.17
      },
      {
        "title": "Ouled Thaleb 2012 Red (Zenata)",
        "points": 91,
        "price": 14,
        "ratio": 6.5
      },
      {
        "title": "Ouled Thaleb 2011 Aït Souala Red (Morocco)",
        "points": 91,
        "price": 25,
        "ratio": 3.64
      },
      {
        "title": "Ouled Thaleb 2010 Syrah (Zenata)",
        "points": 90,
        "price": 17,
        "ratio": 5.29
      },
      {
        "title": "Ouled Thaleb 2012 Syrah (Zenata)",
        "points": 90,
        "price": 18,
        "ratio": 5
      },
      {
        "title": "Ouled Thaleb 2013 Syrah (Zenata)",
        "points": 90,
        "price": 18,
        "ratio": 5
      },
      {
        "title": "Bernard Magrez 2014 Excelcio Syrah-Grenache (Guerrouane)",
        "points": 90,
        "price": 40,
        "ratio": 2.25
      },
      {
        "title": "Ouled Thaleb 2011 Cabernet Sauvignon Grenache (Zenata)",
        "points": 89,
        "price": 14,
        "ratio": 6.36
      },
      {
        "title": "Ouled Thaleb 2013 Red (Zenata)",
        "points": 89,
        "price": 15,
        "ratio": 5.93
      },
      {
        "title": "Ouled Thaleb 2011 Syrah (Zenata)",
        "points": 89,
        "price": 18,
        "ratio": 4.94
      },
      {
        "title": "Ouled Thaleb 2011 Un-Oaked Chardonnay (Zenata)",
        "points": 89,
        "price": 18,
        "ratio": 4.94
      },
      {
        "title": "Ouled Thaleb 2014 Médallion Red (Zenata)",
        "points": 89,
        "price": 18,
        "ratio": 4.94
      },
      {
        "title": "Ouled Thaleb 2012 Aït Soula Red (Morocco)",
        "points": 89,
        "price": 25,
        "ratio": 3.56
      },
      {
        "title": "Ouled Thaleb 2015 Moroccan Red (Zenata)",
        "points": 88,
        "price": 15,
        "ratio": 5.87
      },
      {
        "title": "Ouled Thaleb 2012 Médallion Sauvignon Blanc (Zenata)",
        "points": 88,
        "price": 18,
        "ratio": 4.89
      },
      {
        "title": "Ouled Thaleb 2014 Un-Oaked Chardonnay (Zenata)",
        "points": 88,
        "price": 18,
        "ratio": 4.89
      },
      {
        "title": "Ouled Thaleb 2014 Sauvignon Blanc (Zenata)",
        "points": 88,
        "price": 18,
        "ratio": 4.89
      },
      {
        "title": "Ouled Thaleb 2015 Un-Oaked Chardonnay (Morocco)",
        "points": 88,
        "price": 18,
        "ratio": 4.89
      },
      {
        "title": "Ouled Thaleb 2016 Sauvignon Blanc (Morocco)",
        "points": 88,
        "price": 18,
        "ratio": 4.89
      },
      {
        "title": "Ouled Thaleb 2014 White (Zenata)",
        "points": 87,
        "price": 15,
        "ratio": 5.8
      }
    ],
    "New Zealand": [
      {
        "title": "Squawking Magpie 2014 SQM Gimblett Gravels Cabernets/Merlot Red (Hawke's Bay)",
        "points": 95,
        "price": 79,
        "ratio": 1.2
      },
      {
        "title": "Trinity Hill 2013 Homage Syrah (Hawke's Bay)",
        "points": 95,
        "price": 100,
        "ratio": 0.95
      },
      {
        "title": "Paritua 2014 Syrah (Hawke's Bay)",
        "points": 94,
        "price": 35,
        "ratio": 2.69
      },
      {
        "title": "Escarpment 2014 Pinot Noir (Martinborough)",
        "points": 94,
        "price": 43,
        "ratio": 2.19
      },
      {
        "title": "Ata Rangi 2013 Pinot Noir (Martinborough)",
        "points": 94,
        "price": 55,
        "ratio": 1.71
      },
      {
        "title": "Escarpment 2013 Kupe Single Vineyard Pinot Noir (Martinborough)",
        "points": 94,
        "price": 69,
        "ratio": 1.36
      },
      {
        "title": "Brancott 2013 Chosen Rows Sauvignon Blanc (Marlborough)",
        "points": 94,
        "price": 75,
        "ratio": 1.25
      },
      {
        "title": "Te Mata 2014 Coleraine Red (Hawke's Bay)",
        "points": 94,
        "price": 85,
        "ratio": 1.11
      },
      {
        "title": "Clos Henri 2015 Sauvignon Blanc (Marlborough)",
        "points": 93,
        "price": 25,
        "ratio": 3.72
      },
      {
        "title": "Trinity Hill 2013 Gimblett Gravels Syrah (Hawke's Bay)",
        "points": 93,
        "price": 30,
        "ratio": 3.1
      },
      {
        "title": "Te Mata 2013 Cape Crest Sauvignon Blanc (Hawke's Bay)",
        "points": 93,
        "price": 30,
        "ratio": 3.1
      },
      {
        "title": "Spy Valley 2012 Envoy Single Vineyard Sauvignon Blanc (Marlborough)",
        "points": 93,
        "price": 32,
        "ratio": 2.91
      },
      {
        "title": "Spy Valley 2014 Envoy Johnson Vineyard Sauvignon Blanc (Marlborough)",
        "points": 93,
        "price": 33,
        "ratio": 2.82
      },
      {
        "title": "Man O' War 2008 Dreadnought Syrah (Waiheke Island)",
        "points": 93,
        "price": 35,
        "ratio": 2.66
      },
      {
        "title": "Giesen 2013 The Brothers Late Harvest Sauvignon Blanc (Marlborough)",
        "points": 93,
        "price": 35,
        "ratio": 2.66
      },
      {
        "title": "Esk Valley 2013 Winemaker Reserve Gimblett Gravels Syrah (Hawke's Bay)",
        "points": 93,
        "price": 39,
        "ratio": 2.38
      },
      {
        "title": "Felton Road 2015 Bannockburn Riesling (Central Otago)",
        "points": 93,
        "price": 40,
        "ratio": 2.33
      },
      {
        "title": "Pegasus Bay 2012 Pinot Noir (Waipara Valley)",
        "points": 93,
        "price": 41,
        "ratio": 2.27
      },
      {
        "title": "Neudorf 2010 Moutere Chardonnay (Nelson)",
        "points": 93,
        "price": 42,
        "ratio": 2.21
      },
      {
        "title": "Kumeu River 2011 Hunting Hill Chardonnay (Kumeu)",
        "points": 93,
        "price": 45,
        "ratio": 2.07
      }
    ],
    "Peru": [
      {
        "title": "Intipalka 2012 Valle del Sol Red (Ica)",
        "points": 86,
        "price": 17,
        "ratio": 5.06
      },
      {
        "title": "Tacama 2008 Sinfonía Tannat-Malbec-Petit Verdot Red (Ica)",
        "points": 86,
        "price": 20,
        "ratio": 4.3
      },
      {
        "title": "Intipalka 2013 Valle del Sol Tannat (Ica)",
        "points": 85,
        "price": 14,
        "ratio": 6.07
      },
      {
        "title": "Tacama 2008 Halcón de la Viña Malbec (Ica)",
        "points": 85,
        "price": 15,
        "ratio": 5.67
      },
      {
        "title": "Intipalka 2012 Valle del Sol Reserva Malbec-Merlot (Ica)",
        "points": 85,
        "price": 17,
        "ratio": 5
      },
      {
        "title": "Intipalka 2010 Valle del Sol No 1 Gran Reserva Red (Ica)",
        "points": 85,
        "price": 68,
        "ratio": 1.25
      },
      {
        "title": "Intipalka 2014 Valle del Sol Malbec (Ica)",
        "points": 84,
        "price": 14,
        "ratio": 6
      },
      {
        "title": "Intipalka 2014 Valle del Sol Sauvignon Blanc (Ica)",
        "points": 84,
        "price": 14,
        "ratio": 6
      },
      {
        "title": "Intipalka 2012 Valle del Sol Cabernet Sauvignon-Syrah (Ica)",
        "points": 84,
        "price": 17,
        "ratio": 4.94
      },
      {
        "title": "Tacama 2008 Quantum Petit Verdot (Ica)",
        "points": 84,
        "price": 20,
        "ratio": 4.2
      },
      {
        "title": "Tacama 2010 Semi-Seco Blanco White (Ica)",
        "points": 83,
        "price": 10,
        "ratio": 8.3
      },
      {
        "title": "Tacama 2010 Gran Blanco White (Ica)",
        "points": 82,
        "price": 10,
        "ratio": 8.2
      },
      {
        "title": "Intipalka 2014 Valle del Sol Chardonnay (Ica)",
        "points": 82,
        "price": 14,
        "ratio": 5.86
      },
      {
        "title": "Tacama 2009 Gran Tinto Red (Ica)",
        "points": 81,
        "price": 10,
        "ratio": 8.1
      },
      {
        "title": "Intipalka 2013 Valle del Sol Syrah (Ica)",
        "points": 81,
        "price": 14,
        "ratio": 5.79
      },
      {
        "title": "Tacama 2010 Brut Sparkling (Ica)",
        "points": 80,
        "price": 15,
        "ratio": 5.33
      }
    ],
    "Portugal": [
      {
        "title": "Casa Ferreirinha 2008 Barca-Velha Red (Douro)",
        "points": 100,
        "price": 450,
        "ratio": 0.22
      },
      {
        "title": "Quinta do Noval 2011 Nacional Vintage  (Port)",
        "points": 100,
        "price": 650,
        "ratio": 0.15
      },
      {
        "title": "Taylor Fladgate 2011 Vargellas Vinhas Velhas Vintage  (Port)",
        "points": 99,
        "price": 268,
        "ratio": 0.37
      },
      {
        "title": "Casa Ferreirinha 2004 Barca Velha Red (Douro)",
        "points": 99,
        "price": 426,
        "ratio": 0.23
      },
      {
        "title": "Quinta do Vesuvio 2011 Capela Vintage  (Port)",
        "points": 98,
        "price": 127,
        "ratio": 0.77
      },
      {
        "title": "Blandy's 1969 Bual (Madeira)",
        "points": 98,
        "price": 230,
        "ratio": 0.43
      },
      {
        "title": "Quinta do Noval 2001 Nacional Vintage  (Port)",
        "points": 98,
        "price": 595,
        "ratio": 0.16
      },
      {
        "title": "Taylor Fladgate NV 325 Anniversary  (Port)",
        "points": 97,
        "price": 40,
        "ratio": 2.43
      },
      {
        "title": "Mouchão 2012 Red (Alentejo)",
        "points": 97,
        "price": 75,
        "ratio": 1.29
      },
      {
        "title": "Ferreira 2011 Vintage  (Port)",
        "points": 97,
        "price": 90,
        "ratio": 1.08
      },
      {
        "title": "Quinta do Vale Meão 2012 Red (Douro)",
        "points": 97,
        "price": 90,
        "ratio": 1.08
      },
      {
        "title": "Niepoort 2005 Vintage  (Port)",
        "points": 97,
        "price": 95,
        "ratio": 1.02
      },
      {
        "title": "Cockburn's 2011 Vintage  (Port)",
        "points": 97,
        "price": 95,
        "ratio": 1.02
      },
      {
        "title": "Quinta do Vale Meão 2013 Red (Douro)",
        "points": 97,
        "price": 100,
        "ratio": 0.97
      },
      {
        "title": "Fonseca 2011 Vintage  (Port)",
        "points": 97,
        "price": 116,
        "ratio": 0.84
      },
      {
        "title": "Taylor Fladgate 2011 Vintage  (Port)",
        "points": 97,
        "price": 116,
        "ratio": 0.84
      },
      {
        "title": "W. & J. Graham's 2011 Stone Terraces Vintage  (Port)",
        "points": 97,
        "price": 200,
        "ratio": 0.49
      },
      {
        "title": "Casa Ferreirinha 2007 Reserva Especial Red (Douro)",
        "points": 97,
        "price": 250,
        "ratio": 0.39
      },
      {
        "title": "W. & J. Graham's NV 90-year Old Tawny  (Port)",
        "points": 97,
        "price": 1000,
        "ratio": 0.1
      },
      {
        "title": "Mouchão 2011 Red (Alentejo)",
        "points": 96,
        "price": 55,
        "ratio": 1.75
      }
    ],
    "Romania": [
      {
        "title": "Cramele Recas 2013 Conocul Ambrozy Sauvignon Blanc (Recas)",
        "points": 92,
        "price": 28,
        "ratio": 3.29
      },
      {
        "title": "Davino 2010 Flamboyant Red (Dealu Mare)",
        "points": 90,
        "price": 58,
        "ratio": 1.55
      },
      {
        "title": "Budureasca 2015 T?mâioas? Româneasc? (Dealu Mare)",
        "points": 89,
        "price": 10,
        "ratio": 8.9
      },
      {
        "title": "Budureasca 2013 Origini Reserve Red (Dealu Mare)",
        "points": 89,
        "price": 18,
        "ratio": 4.94
      },
      {
        "title": "Budureasca 2015 Origini Reserve Sauvignon (Dealu Mare)",
        "points": 89,
        "price": 18,
        "ratio": 4.94
      },
      {
        "title": "Cramele Halewood 2009 Kronos Limited Edition Pinot Noir (Dealu Mare)",
        "points": 89,
        "price": 20,
        "ratio": 4.45
      },
      {
        "title": "Domeniile Panciu 2012 Sagio Red (Panciu)",
        "points": 89,
        "price": 20,
        "ratio": 4.45
      },
      {
        "title": "Davino 2010 Purpura Valahica Feteasca Neagra (Dealu Mare)",
        "points": 89,
        "price": 22,
        "ratio": 4.05
      },
      {
        "title": "Davino 2010 Domaine Ceptura Red (Dealu Mare)",
        "points": 89,
        "price": 28,
        "ratio": 3.18
      },
      {
        "title": "Cramele Recas 2012 Dreambird Pinot Grigio (Viile Timisului)",
        "points": 88,
        "price": 8,
        "ratio": 11
      },
      {
        "title": "Murfatlar 2010 Zestrea Demidulce Feteasca Neagra (Murfatlar)",
        "points": 88,
        "price": 9,
        "ratio": 9.78
      },
      {
        "title": "Cramele Halewood 2013 La Umbra Pinot Noir (Dealurile Munteniei)",
        "points": 88,
        "price": 9,
        "ratio": 9.78
      },
      {
        "title": "Cramele Recas 2014 Legendary Estate Series Chardonnay (Romania)",
        "points": 88,
        "price": 9,
        "ratio": 9.78
      },
      {
        "title": "Budureasca 2015 Vine in Flames Pinot Gris (Dealu Mare)",
        "points": 88,
        "price": 9,
        "ratio": 9.78
      },
      {
        "title": "Budureasca 2012 Dealu Mare Pinot Noir (Romania)",
        "points": 88,
        "price": 10,
        "ratio": 8.8
      },
      {
        "title": "Murfatlar 2011 Zestrea Demidulce Muskat Ottonel (Murfatlar)",
        "points": 88,
        "price": 11,
        "ratio": 8
      },
      {
        "title": "Cramele Recas 2011 Paparuda Rezerva Cabernet Sauvignon (Viile Timisului)",
        "points": 88,
        "price": 12,
        "ratio": 7.33
      },
      {
        "title": "Cramele Recas 2011 Paparuda Rezerva Feteasc? Regal? (Viile Timisului)",
        "points": 88,
        "price": 12,
        "ratio": 7.33
      },
      {
        "title": "Jidvei 2014 Ana Owner's Choice Chardonnay (Jidvei)",
        "points": 88,
        "price": 16,
        "ratio": 5.5
      },
      {
        "title": "Cramele Recas 2014 Sole Chardonnay (Recas)",
        "points": 88,
        "price": 17,
        "ratio": 5.18
      }
    ],
    "Serbia": [
      {
        "title": "Budimir 2009 Margus Margi Riesling (Župa)",
        "points": 89,
        "price": 20,
        "ratio": 4.45
      },
      {
        "title": "Budimir 2009 Sub Rosa Red (Župa)",
        "points": 89,
        "price": 40,
        "ratio": 2.23
      },
      {
        "title": "Budimir 2007 Svb Rosa Red (Župa)",
        "points": 89,
        "price": 42,
        "ratio": 2.12
      },
      {
        "title": "Budimir 2013 Župska Tamjanika (Župa)",
        "points": 88,
        "price": 18,
        "ratio": 4.89
      },
      {
        "title": "Agrina 2012 Portuguiser Blauer Portugieser (Fruška Gora)",
        "points": 87,
        "price": 16,
        "ratio": 5.44
      },
      {
        "title": "Milijan Jeli? 2011 Morava (Pocerina)",
        "points": 87,
        "price": 26,
        "ratio": 3.35
      },
      {
        "title": "Agrina 2014 Portuguiser (Fruška Gora)",
        "points": 86,
        "price": 15,
        "ratio": 5.73
      },
      {
        "title": "Budimir 2008 Margus Margi Riesling (Župa)",
        "points": 86,
        "price": 22,
        "ratio": 3.91
      }
    ],
    "Slovakia": [
      {
        "title": "Château Bela 2008 Riesling (Muzla)",
        "points": 87,
        "price": 16,
        "ratio": 5.44
      }
    ],
    "Slovenia": [
      {
        "title": "Movia 2007 Veliko White (Brda)",
        "points": 92,
        "price": 50,
        "ratio": 1.84
      },
      {
        "title": "Movia 2004 Veliko Red (Brda)",
        "points": 91,
        "price": 50,
        "ratio": 1.82
      },
      {
        "title": "Giocato 2015 Sauvignon Blanc (Primorska)",
        "points": 90,
        "price": 12,
        "ratio": 7.5
      },
      {
        "title": "Colliano 2013 Estate Bottled Ribolla Gialla (Goriska Brda)",
        "points": 90,
        "price": 15,
        "ratio": 6
      },
      {
        "title": "Sanctum 2011 Chardonnay (Štajerska)",
        "points": 90,
        "price": 17,
        "ratio": 5.29
      },
      {
        "title": "Štoka 2011 Grganja Vitovska (Kras)",
        "points": 90,
        "price": 23,
        "ratio": 3.91
      },
      {
        "title": "Štoka 2011 Izbrani Teran (Kras)",
        "points": 90,
        "price": 23,
        "ratio": 3.91
      },
      {
        "title": "Kabaj 2011 Ravan Sauvignonasse (Goriska Brda)",
        "points": 90,
        "price": 25,
        "ratio": 3.6
      },
      {
        "title": "Kabaj 2006 Cuvée Morel Red (Goriska Brda)",
        "points": 90,
        "price": 40,
        "ratio": 2.25
      },
      {
        "title": "Movia 2007 Modri Ceglo Pinot Noir (Brda)",
        "points": 90,
        "price": 40,
        "ratio": 2.25
      },
      {
        "title": "Ferdinand 2009 Brutus Ribolla Gialla (Goriska Brda)",
        "points": 90,
        "price": 40,
        "ratio": 2.25
      },
      {
        "title": "Kabaj 2008 Cuvée Morel Red (Goriska Brda)",
        "points": 90,
        "price": 46,
        "ratio": 1.96
      },
      {
        "title": "Bati? 2007 Valentino Sweet Red Merlot-Cabernet Franc (Vipavska Dolina)",
        "points": 90,
        "price": 60,
        "ratio": 1.5
      },
      {
        "title": "Kabaj 2006 Amfora White (Goriska Brda)",
        "points": 90,
        "price": 90,
        "ratio": 1
      },
      {
        "title": "Giocato 2012 Pinot Grigio (Primorska)",
        "points": 89,
        "price": 12,
        "ratio": 7.42
      },
      {
        "title": "Giocato 2014 Pinot Grigio (Primorska)",
        "points": 89,
        "price": 12,
        "ratio": 7.42
      },
      {
        "title": "Giocato 2014 Sauvignon Blanc (Primorska)",
        "points": 89,
        "price": 12,
        "ratio": 7.42
      },
      {
        "title": "Colliano 2013 Cuvée White (Goriska Brda)",
        "points": 89,
        "price": 15,
        "ratio": 5.93
      },
      {
        "title": "Dveri-Pax 2014 Yanez White (Štajerska)",
        "points": 89,
        "price": 18,
        "ratio": 4.94
      },
      {
        "title": "Ferdinand 2014 Rebula Ribolla Gialla (Goriska Brda)",
        "points": 89,
        "price": 22,
        "ratio": 4.05
      }
    ],
    "South Africa": [
      {
        "title": "Klein Constantia 2007 Vin de Constance Muscat (Constantia)",
        "points": 95,
        "price": 90,
        "ratio": 1.06
      },
      {
        "title": "Sadie Family 2011 Columella Red (Swartland)",
        "points": 95,
        "price": 130,
        "ratio": 0.73
      },
      {
        "title": "De Toren 2014 Book 17 XVII Red (Stellenbosch)",
        "points": 95,
        "price": 330,
        "ratio": 0.29
      },
      {
        "title": "Mullineux 2014 Syrah (Swartland)",
        "points": 94,
        "price": 40,
        "ratio": 2.35
      },
      {
        "title": "A.A. Badenhorst Family Wines 2010 Red (Swartland)",
        "points": 94,
        "price": 43,
        "ratio": 2.19
      },
      {
        "title": "A.A. Badenhorst Family Wines 2013 Red (Swartland)",
        "points": 94,
        "price": 43,
        "ratio": 2.19
      },
      {
        "title": "Klein Constantia 2005 Vin de Constance Muscat (Constantia)",
        "points": 94,
        "price": 50,
        "ratio": 1.88
      },
      {
        "title": "De Toren 2009 Fusion V Red (Stellenbosch)",
        "points": 94,
        "price": 50,
        "ratio": 1.88
      },
      {
        "title": "De Toren 2012 Fusion V Red (Stellenbosch)",
        "points": 94,
        "price": 60,
        "ratio": 1.57
      },
      {
        "title": "Mvemve Raats 2011 MR de Compostella Red (Stellenbosch)",
        "points": 94,
        "price": 65,
        "ratio": 1.45
      },
      {
        "title": "Mvemve Raats 2012 MR de Compostella Red (Stellenbosch)",
        "points": 94,
        "price": 65,
        "ratio": 1.45
      },
      {
        "title": "Ernie Els 2004 Limited Release Red (Stellenbosch)",
        "points": 94,
        "price": 93,
        "ratio": 1.01
      },
      {
        "title": "Vilafonté 2012 Series C Red (Paarl)",
        "points": 94,
        "price": 95,
        "ratio": 0.99
      },
      {
        "title": "Abraham Perold 1998 Shiraz (Paarl)",
        "points": 94,
        "price": 155,
        "ratio": 0.61
      },
      {
        "title": "Simonsig 1998 Cabernet Sauvignon (Stellenbosch)",
        "points": 93,
        "price": 15,
        "ratio": 6.2
      },
      {
        "title": "DeMorgenzon 2014 Reserve Chenin Blanc (Stellenbosch)",
        "points": 93,
        "price": 35,
        "ratio": 2.66
      },
      {
        "title": "Hamilton Russell 2012 Pinot Noir (Hemel en Aarde)",
        "points": 93,
        "price": 46,
        "ratio": 2.02
      },
      {
        "title": "Vilafonté 2007 Series C Red (Paarl)",
        "points": 93,
        "price": 50,
        "ratio": 1.86
      },
      {
        "title": "De Toren 2008 Fusion V Red (Stellenbosch)",
        "points": 93,
        "price": 50,
        "ratio": 1.86
      },
      {
        "title": "de Trafford 2008 Straw Wine Chenin Blanc (Stellenbosch)",
        "points": 93,
        "price": 50,
        "ratio": 1.86
      }
    ],
    "Spain": [
      {
        "title": "Emilio Moro 2009 Clon de la Familia  (Ribera del Duero)",
        "points": 98,
        "price": 450,
        "ratio": 0.22
      },
      {
        "title": "Bodegas Gutiérrez de la Vega 1999 Casta Diva Fondillón Sweet Monastrell (Alicante)",
        "points": 97,
        "price": 88,
        "ratio": 1.1
      },
      {
        "title": "Emilio Moro 2010 Malleolus de Valderramiro  (Ribera del Duero)",
        "points": 97,
        "price": 150,
        "ratio": 0.65
      },
      {
        "title": "Emilio Moro 2006 Malleolus de Valderramiro  (Ribera del Duero)",
        "points": 97,
        "price": 180,
        "ratio": 0.54
      },
      {
        "title": "Teso La Monja 2012 Alabaster  (Toro)",
        "points": 97,
        "price": 227,
        "ratio": 0.43
      },
      {
        "title": "Bodegas Roda 2009 Cirsion  (Rioja)",
        "points": 97,
        "price": 250,
        "ratio": 0.39
      },
      {
        "title": "Alvear NV Solera 1927 Pedro Ximénez (Montilla-Moriles)",
        "points": 96,
        "price": 30,
        "ratio": 3.2
      },
      {
        "title": "La Rioja Alta 2005 Gran Reserva 904  (Rioja)",
        "points": 96,
        "price": 50,
        "ratio": 1.92
      },
      {
        "title": "Valderiz 2014 Juegabolos  (Ribera del Duero)",
        "points": 96,
        "price": 50,
        "ratio": 1.92
      },
      {
        "title": "Remírez de Ganuza 2005 Old Vines Unfiltered  (Rioja)",
        "points": 96,
        "price": 82,
        "ratio": 1.17
      },
      {
        "title": "Astrales 2010  Ribera del Duero",
        "points": 96,
        "price": 90,
        "ratio": 1.07
      },
      {
        "title": "Clos Mogador 2010 Red (Priorat)",
        "points": 96,
        "price": 90,
        "ratio": 1.07
      },
      {
        "title": "CVNE 2010 Real de Asúa  (Rioja)",
        "points": 96,
        "price": 100,
        "ratio": 0.96
      },
      {
        "title": "Mauro 2011 VS Tempranillo (Vino de la Tierra de Castilla y León)",
        "points": 96,
        "price": 100,
        "ratio": 0.96
      },
      {
        "title": "Muga 2011 Torre Muga  (Rioja)",
        "points": 96,
        "price": 101,
        "ratio": 0.95
      },
      {
        "title": "La Rioja Alta 2004 Gran Reserva 890  (Rioja)",
        "points": 96,
        "price": 125,
        "ratio": 0.77
      },
      {
        "title": "Emilio Moro 2009 Malleolus de Valderramiro  (Ribera del Duero)",
        "points": 96,
        "price": 130,
        "ratio": 0.74
      },
      {
        "title": "Emilio Moro 2006 Malleolus de Sanchomartin  (Ribera del Duero)",
        "points": 96,
        "price": 204,
        "ratio": 0.47
      },
      {
        "title": "Terroir Al Límit 2014 Les Tosses Carignan (Priorat)",
        "points": 96,
        "price": 290,
        "ratio": 0.33
      },
      {
        "title": "Bodegas Roda 2005 Cirsion  (Rioja)",
        "points": 96,
        "price": 303,
        "ratio": 0.32
      }
    ],
    "Switzerland": [
      {
        "title": "Château d'Auvernier 2012 White (Neuchâtel)",
        "points": 90,
        "price": 21,
        "ratio": 4.29
      },
      {
        "title": "Robert Gilliard 2012 Les Murettes Chasselas (Valais)",
        "points": 90,
        "price": 28,
        "ratio": 3.21
      },
      {
        "title": "Robert Gilliard 2012 Dôle des Monts Pinot Noir-Gamay (Valais)",
        "points": 90,
        "price": 30,
        "ratio": 3
      },
      {
        "title": "Gantenbein 2011 Pinot Noir (Switzerland)",
        "points": 89,
        "price": 160,
        "ratio": 0.56
      },
      {
        "title": "Gantenbein 2012 Pinot Noir (Switzerland)",
        "points": 89,
        "price": 160,
        "ratio": 0.56
      },
      {
        "title": "Delea 2004 Carato Merlot (Ticino)",
        "points": 83,
        "price": 38,
        "ratio": 2.18
      }
    ],
    "Turkey": [
      {
        "title": "Kavaklidere 2010 Pendore Öküzgözü (Aegean)",
        "points": 92,
        "price": 33,
        "ratio": 2.79
      },
      {
        "title": "Kayra 2011 Vintage Single Vineyard Collectible Series #5 Öküzgözü (Elaz??)",
        "points": 92,
        "price": 35,
        "ratio": 2.63
      },
      {
        "title": "Kavaklidere 2010 Pendore Syrah (Aegean)",
        "points": 92,
        "price": 38,
        "ratio": 2.42
      },
      {
        "title": "Vinkara 2011 Reserve Bo?azkere (Ankara)",
        "points": 91,
        "price": 30,
        "ratio": 3.03
      },
      {
        "title": "Urla 2011 Nexus Red (Aegean)",
        "points": 91,
        "price": 34,
        "ratio": 2.68
      },
      {
        "title": "Kavaklidere 2011 Prestige Kalecik Karasi (Ankara)",
        "points": 91,
        "price": 40,
        "ratio": 2.28
      },
      {
        "title": "Vinkara 2014 Yasasin Kalecik Karasi (Ankara)",
        "points": 91,
        "price": 40,
        "ratio": 2.28
      },
      {
        "title": "Vinkara 2013 Dry Kalecik Karasi (Ankara)",
        "points": 90,
        "price": 15,
        "ratio": 6
      },
      {
        "title": "Kavaklidere 2012 Selection Red (Turkey)",
        "points": 90,
        "price": 16,
        "ratio": 5.63
      },
      {
        "title": "Turasan 2014 Bo?azkere (Cappadocia)",
        "points": 90,
        "price": 19,
        "ratio": 4.74
      },
      {
        "title": "Vinkara 2013 Winehouse Öküzgözü (Ankara)",
        "points": 90,
        "price": 23,
        "ratio": 3.91
      },
      {
        "title": "Urla 2010 Vourla Red (Aegean)",
        "points": 90,
        "price": 24,
        "ratio": 3.75
      },
      {
        "title": "Vinkara 2012 Reserve Kalecik Karasi (Ankara)",
        "points": 90,
        "price": 27,
        "ratio": 3.33
      },
      {
        "title": "Vinkara 2013 Reserve Kalecik Karasi (Ankara)",
        "points": 90,
        "price": 27,
        "ratio": 3.33
      },
      {
        "title": "Vinkara 2014 Reserve Kalecik Karasi (Ankara)",
        "points": 90,
        "price": 27,
        "ratio": 3.33
      },
      {
        "title": "Pasaeli 2012 K2 Red (Aegean)",
        "points": 90,
        "price": 28,
        "ratio": 3.21
      },
      {
        "title": "Kayra 2012 Öküzgözü (Elaz??)",
        "points": 90,
        "price": 29,
        "ratio": 3.1
      },
      {
        "title": "Sevilen 2007 Premium Tepe Vineyard Syrah-Merlot (Aegean)",
        "points": 90,
        "price": 30,
        "ratio": 3
      },
      {
        "title": "Suvla 2011 Reserve Single Vineyard Bozokbag Cabernet Sauvignon (Thrace)",
        "points": 90,
        "price": 39,
        "ratio": 2.31
      },
      {
        "title": "Suvla 2011 Reserve Single Vineyard Bozokbag Syrah (Thrace)",
        "points": 90,
        "price": 39,
        "ratio": 2.31
      }
    ],
    "Ukraine": [
      {
        "title": "Koblevo NV Reserve Riesling (Ukraine)",
        "points": 88,
        "price": 9,
        "ratio": 9.78
      },
      {
        "title": "Koblevo NV Royal Reserve Muscat Hamburg (Ukraine)",
        "points": 86,
        "price": 9,
        "ratio": 9.56
      },
      {
        "title": "Koblevo NV Kagor Reserve Bastardo (Ukraine)",
        "points": 85,
        "price": 9,
        "ratio": 9.44
      },
      {
        "title": "Koblevo NV Reserve Chardonnay (Ukraine)",
        "points": 85,
        "price": 9,
        "ratio": 9.44
      },
      {
        "title": "Côtnar Hills 2014 Semi-Dry Rosé (Ukraine)",
        "points": 84,
        "price": 6,
        "ratio": 14
      },
      {
        "title": "Côtnar Hills 2014 Cabernet Sauvignon (Ukraine)",
        "points": 84,
        "price": 6,
        "ratio": 14
      },
      {
        "title": "Côtnar Hills 2014 Merlot (Ukraine)",
        "points": 84,
        "price": 6,
        "ratio": 14
      },
      {
        "title": "Koblevo NV Reserve Merlot (Ukraine)",
        "points": 84,
        "price": 9,
        "ratio": 9.33
      },
      {
        "title": "Artemovsk 2008 KrimSekt White Collection Semi-Dry Sparkling (Ukraine)",
        "points": 84,
        "price": 13,
        "ratio": 6.46
      },
      {
        "title": "Marengo NV Brut Sparkling (Ukraine)",
        "points": 83,
        "price": 10,
        "ratio": 8.3
      },
      {
        "title": "Marengo NV Semi-Sweet Bianco Sparkling (Ukraine)",
        "points": 83,
        "price": 10,
        "ratio": 8.3
      },
      {
        "title": "Artemovsk 2008 KrimSekt White Collection Brut Sparkling (Ukraine)",
        "points": 83,
        "price": 13,
        "ratio": 6.38
      },
      {
        "title": "Marengo NV Rose Sparkling (Ukraine)",
        "points": 82,
        "price": 10,
        "ratio": 8.2
      },
      {
        "title": "Marengo NV Semi-Dry Sparkling (Ukraine)",
        "points": 82,
        "price": 10,
        "ratio": 8.2
      }
    ],
    "Uruguay": [
      {
        "title": "Bouza 2015 B6 Parcela Única Tannat (Canelones)",
        "points": 92,
        "price": 43,
        "ratio": 2.14
      },
      {
        "title": "Bouza 2015 Reserva Tannat (Montevideo)",
        "points": 91,
        "price": 22,
        "ratio": 4.14
      },
      {
        "title": "Narbona NV Blend 002 Limited Edition Tannat-Cabernet Franc (Uruguay)",
        "points": 91,
        "price": 22,
        "ratio": 4.14
      },
      {
        "title": "Bouza 2017 Albariño (Montevideo)",
        "points": 91,
        "price": 25,
        "ratio": 3.64
      },
      {
        "title": "Montes Toscanini 2013 Gran Premium Unfiltered Tannat (Canelones)",
        "points": 91,
        "price": 44,
        "ratio": 2.07
      },
      {
        "title": "Pizzorno 2011 Primo Red (Uruguay)",
        "points": 91,
        "price": 50,
        "ratio": 1.82
      },
      {
        "title": "Bouza 2015 Monte Vide Eu Tannat-Merlot-Tempranillo Red (Montevideo)",
        "points": 91,
        "price": 60,
        "ratio": 1.52
      },
      {
        "title": "Pizzorno 2013 Select Blend Reserva Red (Canelones)",
        "points": 90,
        "price": 20,
        "ratio": 4.5
      },
      {
        "title": "Familia Deicas 2015 Deicas Tannat (Uruguay)",
        "points": 90,
        "price": 24,
        "ratio": 3.75
      },
      {
        "title": "Bouza 2013 Albariño (Montevideo)",
        "points": 90,
        "price": 25,
        "ratio": 3.6
      },
      {
        "title": "Pueblo del Sol 2009 30 Barricas Edición Limitada Tannat (Juanico)",
        "points": 90,
        "price": 29,
        "ratio": 3.1
      },
      {
        "title": "Garzón 2015 Single Vineyard Pinot Noir (Uruguay)",
        "points": 90,
        "price": 30,
        "ratio": 3
      },
      {
        "title": "Narbona 2013 Roble Tannat (Uruguay)",
        "points": 90,
        "price": 35,
        "ratio": 2.57
      },
      {
        "title": "Familia Deicas 2004 Preludio Barrel Select Lote N 77 Red (Juanico)",
        "points": 90,
        "price": 45,
        "ratio": 2
      },
      {
        "title": "Pisano 2007 Etxe Oneko Fortified Sweet Red Tannat (Progreso)",
        "points": 90,
        "price": 46,
        "ratio": 1.96
      },
      {
        "title": "Familia Deicas 2008 Massimo Deicas Tannat (Juanico)",
        "points": 90,
        "price": 130,
        "ratio": 0.69
      },
      {
        "title": "Ariano Hermanos 2013 Don Adelio Reserve Tannat-Cabernet Franc (Canelones)",
        "points": 89,
        "price": 16,
        "ratio": 5.56
      },
      {
        "title": "Artesana 2015 Tannat (Canelones)",
        "points": 89,
        "price": 18,
        "ratio": 4.94
      },
      {
        "title": "Bouza 2014 Reserva Tannat (Uruguay)",
        "points": 89,
        "price": 20,
        "ratio": 4.45
      },
      {
        "title": "Gimenez Mendez 2015 Las Brujas Tannat (Canelones)",
        "points": 89,
        "price": 22,
        "ratio": 4.05
      }
    ],
    "US": [
      {
        "title": "Charles Smith 2006 Royal City Syrah (Columbia Valley (WA))",
        "points": 100,
        "price": 80,
        "ratio": 1.25
      },
      {
        "title": "Cayuse 2008 Bionic Frog Syrah (Walla Walla Valley (WA))",
        "points": 100,
        "price": 80,
        "ratio": 1.25
      },
      {
        "title": "Cardinale 2006 Cabernet Sauvignon (Napa Valley)",
        "points": 100,
        "price": 200,
        "ratio": 0.5
      },
      {
        "title": "Verité 2007 La Muse Red (Sonoma County)",
        "points": 100,
        "price": 450,
        "ratio": 0.22
      },
      {
        "title": "Failla 2010 Estate Vineyard Chardonnay (Sonoma Coast)",
        "points": 99,
        "price": 44,
        "ratio": 2.25
      },
      {
        "title": "Cayuse 2009 En Chamberlin Vineyard Syrah (Walla Walla Valley (OR))",
        "points": 99,
        "price": 75,
        "ratio": 1.32
      },
      {
        "title": "Williams Selyem 2010 Hirsch Vineyard Pinot Noir (Sonoma Coast)",
        "points": 99,
        "price": 75,
        "ratio": 1.32
      },
      {
        "title": "Cayuse 2011 En Chamberlin Vineyard Syrah (Walla Walla Valley (OR))",
        "points": 99,
        "price": 75,
        "ratio": 1.32
      },
      {
        "title": "Williams Selyem 2009 Precious Mountain Vineyard Pinot Noir (Sonoma Coast)",
        "points": 99,
        "price": 94,
        "ratio": 1.05
      },
      {
        "title": "Trefethen 2005 Reserve Cabernet Sauvignon (Oak Knoll District)",
        "points": 99,
        "price": 100,
        "ratio": 0.99
      },
      {
        "title": "Quilceda Creek 2008 Cabernet Sauvignon (Columbia Valley (WA))",
        "points": 99,
        "price": 125,
        "ratio": 0.79
      },
      {
        "title": "Venge 2008 Family Reserve Cabernet Sauvignon (Oakville)",
        "points": 99,
        "price": 125,
        "ratio": 0.79
      },
      {
        "title": "David Arthur 2009 Elevation 1147 Estate Cabernet Sauvignon (Napa Valley)",
        "points": 99,
        "price": 150,
        "ratio": 0.66
      },
      {
        "title": "Alpha Omega 2012 Stagecoach Vineyard Cabernet Sauvignon (Atlas Peak)",
        "points": 99,
        "price": 250,
        "ratio": 0.4
      },
      {
        "title": "Colgin 2008 IX Estate Red (Napa Valley)",
        "points": 99,
        "price": 290,
        "ratio": 0.34
      },
      {
        "title": "Alpha Omega 2012 ERA Red (Napa Valley)",
        "points": 99,
        "price": 300,
        "ratio": 0.33
      },
      {
        "title": "Pirouette 2008 Red Wine Red (Columbia Valley (WA))",
        "points": 98,
        "price": 50,
        "ratio": 1.96
      },
      {
        "title": "Gramercy 2010 Lagniappe Syrah (Columbia Valley (WA))",
        "points": 98,
        "price": 55,
        "ratio": 1.78
      },
      {
        "title": "Mt. Brave 2008 Merlot (Mount Veeder)",
        "points": 98,
        "price": 60,
        "ratio": 1.63
      },
      {
        "title": "Williams Selyem 2013 Westside Road Neighbors Pinot Noir (Russian River Valley)",
        "points": 98,
        "price": 69,
        "ratio": 1.42
      }
    ]
  }
;
