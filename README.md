# VVine
Proposé par Noah Graells - Maxime Grava - Sven Rouvinez

![](./images/application.png)


Ce projet est la création d'une visualisation d'information en utilisant les connaissances assimilées durant le cours "Visualisation de l'information".

Le but de cet visualisation est de permettre aux amateurs de vin de découvrir la culture du vin et d'en retirer le meilleur, au-delà de leur frontière.

Nous avons décidé de représenter les meilleurs vins dans une série de pays et l'évolution de leur note moyenne dans le temps. Il y a un plusieurs visualisations, ce document va  synthétiser les données utilisées, énumérer et motiver les choix.

## Données
Le dataset [www.kaggle.com/zynicide/wine-reviews](https://www.kaggle.com/zynicide/wine-reviews) a été utilisé pour ce travail. Il a néanmoins demandé de retravailler les données afin de limiter sa taille pour avoir une application réactive. Les champs intéressants sont:
- Country: le pays dans lequel se trouve le vin
- Points (0 à 100): ce champ a été traités avoir d'avoir la moyenne des notes de tous les vins selon le pays
- Price: le prix du vin en dollar
- Province: la région de la provenance du vin
- Variety: le type de raisin utilisé pour le vin
- Title: le nom du vin et contient la date


## Visualisations
L'application est déployée et prête à être utilisée à l'adresse [https://sven-hash.gitlab.io/vvine/](https://sven-hash.gitlab.io/vvine/)

### Big Picture
Pour commencer nous avons décidé de séparer l'information en quatre parties afin de pouvoir simplement et rapidement avoir une vue d'ensemble des informations. Lorsque l'utilisateur se rend sur le site, il est invité à sélectionner un pays en cliquant dessus pour commencer l'aventure.

La séparation de notre application en 4 parties permet d'avoir toute l'information au premier coup d'oeil et ensuite d'animer les rectangles pour signifier l'action de l'utilisateur.

#### Choix des couleurs
Nous avons tenté de lier des couleurs qui rappellent le vin, tout en permettant d'être perçues par le plus grand nombre de personne indépendamment de leur handicap visuel comme le montre l'image ci-dessous.

![](./images/images.png)



#### Carte
Cette visualisation est le point d'entrée de l'application. Plus un pays possède de vins, plus le rouge sera foncé. Pour signifier la sélection d'un pays, la couleur jaune (Goldfinch) a été choisie car après le test de plusieurs couleurs c'est celle-ci qui donnait le plus de satisfaction quant à la différence avec le rouge et la signification de la sélection. Il est aussi possible de zoomer sur la carte afin de simplifier la sélection du pays et en passant la souris sur le pays le nombre de vins disponibles dans ce pays est indiqué.

Après avoir cliqué sur un pays, le titre change et indique le nombre de vins. Les 3 autres rectangles s'animent. Ils sont détaillés aux prochaines sections.

Le choix d'une carte du monde est défini par la nature des données que l'on veut représenter, dans notre cas des pays.

#### Meilleurs vins
L'affichage des meilleurs vins en fonction de leur prix est représenté par un graphique de type "bar". L'utilisateur ou l'utilisatrice va pourvoir savoir quel sont les 10 meilleurs vins les moins chers, en survolant à l'aide de sa souris la "bar", le prix va apparaître.
Il peut aussi choisir de lister parmi les 20 meilleurs vins, les 10 moins chers ou plus cher en appuyant sur "Ascending" ou "Descending".

Nous avons décidé d'utiliser un "bar chart" car il nous permet une comparaison visuelle rapide des prix des vins.

#### Moyenne des notes par années
Ce graphique de type "line" permet d'indiquer à l'internaute la moyenne des notes de tous les vins d'années en années selon le pays sélectionné.
En plus de cela, la sélection d'un intervalle d'année est disponible pour permettre de se concentrer sur une seule partie.

Le "line graph" est le plus sensé car nous représentons ici une série temporelle.

#### Variété et province les plus représentées
Ces graphiques "pie" permettent d'afficher la variété de cépage la plus cultivée dans le pays sélectionné ainsi que la province dont la majorité des vins proviennent. Ces informations sont utiles si une personne ne connaît pas le type de vin ou la région pour lesquels un pays est connu .

Il était important pour nous de montrer le cépage principal du pays et la région principale en proportion à tous les autres et c'est donc pour cela que nous avons utilisé une variante du "pie chart", le "doughnut" (lois de la Gestalt)

#### Aide
L'aide, représenté par un "?" permet à l'utilisateur du site d'obtenir des explications.

## Critères
Il est important de pouvoir valider notre application. Nous allons pour cela utiliser le mantra de Shneiderman et les règles d'utilisabilité

### Mantra
> Overview first,zoom and filter,then details-on-demand

- Overview: la carte permet d'avoir un aperçu des pays sélectionnables
- Zoom: après sélection du pays, les graphiques s'affichent
- Filter: il est possible de filtrer par prix et de sélectionner les années
- Details-on-demand: en passant le curseur de la souris sur les graphiques des informations précises apparaissent

### Règles d'utilisabilité
- Bliss: s'il sait utiliser une souris, l'utilisateur peut utiliser le site
- Distractions: la personne peut faire plusieurs choses en même temps et quand même utiliser le site
- Flow: lors de la sélection d'un pays, les graphiques s'animent afin que l'utilisateur s'en rende compte
- Documentation: le fonctionnement est expliqué en cliquant sur le "?"
- Least surprise: après la sélection, toutes les données sur le pays sont affichées
- Transparency: l'utilisateur peut tout le temps modifier le pays
- Modelessness: la sélection du pays est permise, indépendamment de l'état de l'application
- Seven: il n'est pas nécessaire de se souvenir du pays choisi car il renseigné
- Reversibility: l'internaute peut changer le pays sans devoir recharger la page
- Confirmation: pas de "prompt" inutile
- Failure: l'utilisateur ne peut sélectionner un pays sans vin
- Silence: les données des graphiques s'affichent ou changent lors d'une nouvelle sélection sans demander de confirmation à l'utilisateur
- Automation: il n'y a pas de champ à remplir 
- Defaults: par défaut aucun pays n'est sélectionné ce qui permet de découvrir l'application
- Respect: un seul point d'entrée pour commencer à utiliser l'application
- Predictability: l'interface est simple
- Reality: pas de tests à proprement parler mais des retours de notre entourage qui ont pu tester


## Conclusion
Ce projet était intéressant car il a permis de mettre en place des éléments de visualisation de manière pratique et d'obtenir un résultat afin de le challenger selon les points vu en cours.
Nous nous sommes rendus compte durant ce travail de l'importance du choix du dataset, il nous aurait fallut normaliser car des pays ont beaucoup de vins et d'autres juste quelques uns, néanmoins nous avons pu visualiser et choisir les meilleures représentations selon nous avec les données à notre disposition.

## Technologies
- [Chart.js](https://www.chartjs.org/)
- [noUiSlider](https://refreshless.com/nouislider/)
- [HIGHCHARTS](https://www.highcharts.com/)

